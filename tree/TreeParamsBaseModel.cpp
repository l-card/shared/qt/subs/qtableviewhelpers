#include "TreeParamsBaseModel.h"
#include "TreeParamsBaseItem.h"
#include <QTreeView>

TreeParamsBaseModel::TreeParamsBaseModel(QObject *parent) : QAbstractItemModel{parent} {


}

void TreeParamsBaseModel::initView(QTreeView *view) {
    view->setModel(this);
    for (int col {0}; col < m_params.size(); ++col) {
        QAbstractItemDelegate *deletgate {m_params.at(col)->createDelegate(view)};
        if (deletgate)
            view->setItemDelegateForColumn(col, deletgate);
        if (m_params.at(col)->columnWidth() > 0)
            view->setColumnWidth(col, m_params.at(col)->columnWidth());
    }
}

QModelIndex TreeParamsBaseModel::index(int row, int column, const QModelIndex &parent) const {
    QModelIndex ret;
    TreeParamsBaseItem *parItem {treeItem(parent)};
    if (parItem) {
        if (row < parItem->treeChildrenCount())
            ret = createIndex(row, column, parItem->treeChild(row));
    }
    return ret;
}

QModelIndex TreeParamsBaseModel::parent(const QModelIndex &index) const {
    QModelIndex ret;
    TreeParamsBaseItem *parent {treeItem(index)->treeParent()};
    if (parent) {
        ret = treeItemIndex(parent);
    }
    return ret;
}

int TreeParamsBaseModel::rowCount(const QModelIndex &parent) const {
    int ret {0};
    TreeParamsBaseItem *parItem {treeItem(parent)};
    if (parItem)
        ret = parItem->treeChildrenCount();
    return ret;
}

int TreeParamsBaseModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return m_params.size();
}

QVariant TreeParamsBaseModel::data(const QModelIndex &index, int role) const {
    QVariant ret;
    if (index.isValid()) {
        TreeParamsBaseItem *curItem {treeItem(index)};
        if (curItem) {
            ret = m_params.at(index.column())->data(curItem, role);
        }
    }
    return ret;
}

Qt::ItemFlags TreeParamsBaseModel::flags(const QModelIndex &index) const {
    Qt::ItemFlags flags {Qt::ItemFlag::NoItemFlags};
    if (index.isValid()) {
        TreeParamsBaseItem *curItem = treeItem(index);
        if (curItem) {
            flags = m_params.at(index.column())->flags(curItem);
        }
    }
    return flags;
}

bool TreeParamsBaseModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    bool ret {false};
    if (index.isValid()) {
        TreeParamsBaseItem *curItem {treeItem(index)};
        if (curItem) {
            Param *par {m_params.at(index.column())};
            ret = par->setData(curItem, value, role);
            if (ret) {
                if (par->itemParamUpdateReqired()) {
                    Q_EMIT dataChanged(treeItemIndex(curItem, 0), treeItemIndex(curItem, m_params.size()-1));
                } else if (ret) {
                    Q_EMIT dataChanged(index, index);
                }
            }
        }
    }
    return ret;
}

QVariant TreeParamsBaseModel::headerData(int section, Qt::Orientation orientation, int role) const {
    QVariant ret;
    Q_UNUSED(orientation);
    if (section < m_params.size()) {
        if (role == Qt::DisplayRole) {
            ret = m_params.at(section)->name();
        } else if (role == Qt::ToolTipRole) {
            ret = m_params.at(section)->paramToolTip();
        }
    }
    return ret;
}

TreeParamsBaseItem *TreeParamsBaseModel::treeItem(const QModelIndex &index) const {
    return index.isValid() ? static_cast<TreeParamsBaseItem *>(index.internalPointer()) : treeRoot();
}

QModelIndex TreeParamsBaseModel::treeItemIndex(TreeParamsBaseItem *curItem, int col) const {
    return curItem->treeParent() ? createIndex(treeItemRow(curItem), col, curItem) : QModelIndex();
}

int TreeParamsBaseModel::treeItemRow(TreeParamsBaseItem *curItem) const {
    int ret {0};
    TreeParamsBaseItem *parItem {curItem->treeParent()};
    if (parItem) {
        int children_cnt {curItem->treeParent()->treeChildrenCount()};
        for (int i {0}; i < children_cnt; ++i) {
            if (curItem->treeParent()->treeChild(i) == curItem) {
                ret = i;
                break;
            }
        }
    }
    return ret;
}

QList<TreeParamsBaseItem *> TreeParamsBaseModel::getAllItems(int max_depth, int min_depth) {
    QList<TreeParamsBaseItem *> ret;
    treeRoot()->getChildrenRecursive(ret, max_depth, min_depth);
    return ret;
}

void TreeParamsBaseModel::paramAdd(TreeParamsBaseModel::Param *param) {
    m_params.append(param);
}

TreeParamsBaseModel::~TreeParamsBaseModel() {
    qDeleteAll(m_params);
}
