#include "TreeParamsSelectedItem.h"

TreeParamsSelectedItem::TreeParamsSelectedItem(Qt::CheckState init_state, bool checkEn) :
    m_checkState(init_state), m_checkEnabled(checkEn) {

}

void TreeParamsSelectedItem::setChecked(bool checked) {
    setCheckState(checked ? Qt::Checked : Qt::Unchecked, true, true);
    TreeParamsSelectedItem *parent = dynamic_cast<TreeParamsSelectedItem *>(treeParent());
    if (parent)
        parent->childStateChanged();
}

void TreeParamsSelectedItem::setCheckEnabled(bool en) {
    if (en != m_checkEnabled) {
        m_checkEnabled = en;
        Q_EMIT checkStateChanged();
        TreeParamsSelectedItem *parent = dynamic_cast<TreeParamsSelectedItem *>(treeParent());
        if (parent)
            parent->childStateChanged();
    }
}

void TreeParamsSelectedItem::childStateChanged() {
    setCheckFromChildren();
}

void TreeParamsSelectedItem::setCheckFromChildren() {
    Qt::CheckState state = Qt::PartiallyChecked;
    bool state_init = false;


    for (int i = 0; i < treeChildrenCount(); i++) {
        TreeParamsSelectedItem *child = dynamic_cast<TreeParamsSelectedItem *>(treeChild(i));
        if (child && child->checkEnabled()) {
            if (!state_init) {
                state_init = true;
                state = child->checkState();
            } else {
                if (state != child->checkState()) {
                    state = Qt::PartiallyChecked;
                }
            }
        }
    }

    if (state_init && (state != checkState())) {
        setCheckState(state, false, true);
        TreeParamsSelectedItem *parent = dynamic_cast<TreeParamsSelectedItem *>(treeParent());
        if (parent)
            parent->childStateChanged();
    }
}

void TreeParamsSelectedItem::setCheckState(Qt::CheckState state, bool childUpdate, bool emitSinals) {
    if (checkState() != state) {
        if (protSetCheckState(state)) {
            m_checkState = state;
            if (emitSinals)
                Q_EMIT checkStateChanged();
            if (childUpdate && (state != Qt::PartiallyChecked)) {
                for (int i = 0; i < treeChildrenCount(); i++) {
                    TreeParamsSelectedItem *child = dynamic_cast<TreeParamsSelectedItem *>(treeChild(i));
                    if (child) {
                        child->setCheckState(state, true, true);
                    }
                }
            }
        }
    }
}
