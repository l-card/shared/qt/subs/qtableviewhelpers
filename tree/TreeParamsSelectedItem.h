#ifndef TREESELECTEDITEM_H
#define TREESELECTEDITEM_H

#include <QObject>
#include "TreeParamsBaseItem.h"

/* Элемент дерева, основанного на параметрах, позволяющий выбрать его разрешение.
 * При этом разрешение ветвей определяется признаком разрешения его дочерних элементов,
 * и ручное разрешение/запрет ветви приводит к разрешению/запрету всех дочерних
 * элементов.
 * Смена состояния выбора оповещается сигналом checkStateChanged(), чтобы можно
 * было по нему в модели послать сигнал dataChanged() для оповещения представлений */
class TreeParamsSelectedItem : public QObject, public TreeParamsBaseItem {
    Q_OBJECT
public:
    TreeParamsSelectedItem(Qt::CheckState init_state = Qt::Unchecked, bool checkEn = true);

    Qt::CheckState checkState() const {return m_checkState;}
    void setChecked(bool checked);
    void setCheckEnabled(bool en);

    virtual bool checkEnabled() const {return m_checkEnabled;}
    virtual void childStateChanged();
Q_SIGNALS:
    void checkStateChanged();
protected:
    virtual bool protSetCheckState(Qt::CheckState state) {
        Q_UNUSED(state);
        return true;
    }
private:
    void setCheckFromChildren();
    void setCheckState(Qt::CheckState state, bool childUpdate, bool emitSinals);

    Qt::CheckState m_checkState;
    bool m_checkEnabled;
};

#endif // TREESELECTEDITEM_H
