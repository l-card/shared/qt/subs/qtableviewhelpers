#ifndef TREEPARAMSBASEMODEL_H
#define TREEPARAMSBASEMODEL_H

#include <QAbstractItemModel>
#include "../table/params/TableParameter.h"
class TreeParamsBaseItem;
class QTreeView;

class TreeParamsBaseModel : public QAbstractItemModel {
    Q_OBJECT
public:
    TreeParamsBaseModel(QObject *parent = Q_NULLPTR);
    ~TreeParamsBaseModel();

    void initView(QTreeView *view);

    /* стандартный интерфейс для модели */
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData (const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    TreeParamsBaseItem *treeItem(const QModelIndex &index) const;

    QModelIndex treeItemIndex(TreeParamsBaseItem *curItem, int col = 0) const;
    int treeItemRow(TreeParamsBaseItem *curItem) const;


    typedef TableParameter<TreeParamsBaseItem *> Param;

    int paramCnt() const {return m_params.size();}

    QList<TreeParamsBaseItem *> getAllItems(int max_depth = 100, int min_depth = 0);
protected:
    void paramAdd(Param *param);

    virtual TreeParamsBaseItem *treeRoot() const = 0;

private:
    QList<Param *> m_params;
};


#endif // TREEPARAMSBASEMODEL_H
