#ifndef TREEPARAMSBASEITEM_H
#define TREEPARAMSBASEITEM_H

#include <QList>

class TreeParamsBaseItem {
public:
    virtual ~TreeParamsBaseItem();
    virtual TreeParamsBaseItem *treeParent() const = 0;
    virtual int treeChildrenCount() const  = 0;
    virtual TreeParamsBaseItem *treeChild(int idx) const = 0;


    void getChildrenRecursive(QList<TreeParamsBaseItem *> &list, int max_depth = 100, int min_depth = 0) const {
        if (max_depth > 0) {
            --max_depth;
            --min_depth;

            for (int i {0}; i < treeChildrenCount(); ++i) {
                TreeParamsBaseItem *child {treeChild(i)};
                if (min_depth <= 0)
                    list.append(child);
                if (max_depth > 0) {
                    child->getChildrenRecursive(list, max_depth, min_depth);
                }
            }
        }
    }
};

#endif // TREEPARAMSBASEITEM_H
