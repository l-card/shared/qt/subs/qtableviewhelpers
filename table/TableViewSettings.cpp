#include "TableViewSettings.h"
#include <QTableView>
#include <QHeaderView>
#include <QApplication>
#include <QSettings>
#include <QStringBuilder>

#include "actions/TableCopyAction.h"

TableViewSettings *TableViewSettings::instance() {
    static TableViewSettings settings;
    return &settings;
}

QBrush TableViewSettings::readonlyTableElementBrush() const {
    QPalette palette {QApplication::palette()};
    palette.setCurrentColorGroup(QPalette::ColorGroup::Disabled);
    return palette.window();
}

QBrush TableViewSettings::notAssignedTableElementBrush() const {
    QPalette palette {QApplication::palette()};
    palette.setCurrentColorGroup(QPalette::ColorGroup::Inactive);
    return palette.window();
}



void TableViewSettings::initDataView(QTableView *view, QString name, const QSettings &set, InitFlags flags) const {

    if (!(flags & InitFlag::InitNoAction)) {
        TableCopyAction *copy_act {new TableCopyAction{view}};
        connect(this, &TableViewSettings::languageChanged, copy_act, &TableCopyAction::retranslate);
        view->addAction(copy_act);
    }

    view->setAlternatingRowColors(true);
    view->verticalHeader()->setVisible(false);    


    if (!name.isEmpty()) {
        view->setObjectName(name);
        if (view->model()) {
            restoreColumnsState(view->horizontalHeader(), set, name);
        }
    }
}

void TableViewSettings::saveDataView(const QTableView *view,  QSettings &set) const {
    QString name {view->objectName()};
    if (!name.isEmpty()) {
        saveColumnsState(view->horizontalHeader(), set, name);
    }
}

void TableViewSettings::restoreColumnsState(QHeaderView *hdr, const QSettings &set, const QString &name) {
    QByteArray state;
    const int cnt {hdr->count()};
    if (cnt > 0) {
        /* сперва проверяем последнее состояние, если оно действительно, то загружаем
         * его */
        const int lastColCnt {set.value(name % QLatin1String("ColumnCnt")).toInt()};
        if (lastColCnt == cnt) {
            state = set.value(name % QLatin1String("ColumnsState")).toByteArray();
        } else {
            /* иначе пробуем загрузить именнованную группу по числу столбцов */
            state = set.value(name % QLatin1String("ColumnsState") + QString::number(cnt)).toByteArray();
        }
    }

    if (!state.isEmpty())
        hdr->restoreState(state);
}

void TableViewSettings::saveColumnsState(const QHeaderView *hdr, QSettings &set, const QString &name) {
    const int cnt {hdr->count()};
    if (cnt > 0) {
        /* сохраняем кол-во столбцов и их состояние для текущий параметров
            (оставлено для совместимости) */
        set.setValue(name % QLatin1String("ColumnCnt"), hdr->count());
        set.setValue(name % QLatin1String("ColumnsState"), hdr->saveState());
        /* также сохраняем запись в группе, соответствующей числу столбцов,
         * для возможности загрузки нескольких вариантов */
        set.setValue(name % QLatin1String("ColumnsState") + QString::number(cnt), hdr->saveState());
    }

}

void TableViewSettings::retranslate() {
    Q_EMIT languageChanged();
}

TableViewSettings::TableViewSettings() :
    m_disabledTableElemBrush{Qt::GlobalColor::lightGray},
    m_positiveTableElemBrush{Qt::GlobalColor::darkGreen},
    m_negativeTableElemBrush{Qt::GlobalColor::red},
    m_progressTableElemBrush{Qt::GlobalColor::blue} {

}
