#ifndef TABLEPARAMMODELIFACE_H
#define TABLEPARAMMODELIFACE_H

class QAbstractItemDelegate;
class QTableView;
class QAbstractItemModel;

class TableParamsModelIface {
public:
    virtual ~TableParamsModelIface();
    /* обновление всех параметров указанного элемента */
    virtual void updateItem(int item_idx) {paramItemsUpdate(item_idx, item_idx);}
    /* обновление только указанного диапазона параметров указанного элемента */
    virtual void updateItemParams(int item_idx, int param_idx_first, int param_idx_last) = 0;
    /* обновление только указанного параметра указанного элемента */
    virtual void updateItemParam(int item_idx, int param_idx) {
        updateItemParams(item_idx, param_idx, param_idx);
    }




    virtual void paramItemsBeginInsert(int first, int last) = 0;
    virtual void paramItemsEndInsert() = 0;
    virtual void paramItemsBeginRemove(int first, int last) = 0;
    virtual void paramItemsEndRemove() = 0;
    virtual void paramItemsUpdate(int first, int last) = 0;


    virtual int paramDefaultColumnWidth(int param_idx) = 0;
    virtual QAbstractItemDelegate *paramCreateDelegate(int param_idx, QTableView *view) = 0;
    virtual QAbstractItemModel *paramItemModel() = 0;
};

#endif // TABLEPARAMMODELIFACE_H

