#ifndef TABLEPARAMSUPDATEDMODEL_H
#define TABLEPARAMSUPDATEDMODEL_H

#include "TableParamsModelHelper.h"
#include <QList>

template <typename ParamItem, typename UpdateItem> class TableParamsUpdatedModel :
    public TableParamsModelHelper<ParamItem> {


protected:
    virtual ParamItem *paramUpdateCreateItem(UpdateItem upitem) = 0;
    virtual void paramUpdateItemData(ParamItem *item, UpdateItem upitem) = 0;
    virtual void paramUpdateRemItem(int item_pos) = 0;
    virtual bool paramUpdateItemIsEqual(ParamItem *item, UpdateItem upitem) = 0;
    virtual bool paramUpdateItemIsGreater(ParamItem *item, UpdateItem upitem) = 0;



    void paramUpdateItemList(TableParamsModelIface *model, QList<UpdateItem> newItems) {
        int item_pos = 0, new_pos = 0;
        enum State {
            StateNone,
            StateRemove,
            StateAppend,
            StateChanged
        } prev_stat, stat;
        int rep_cnt = 0;
        prev_stat = stat = StateNone;


        do {
            UpdateItem nextNewRec = new_pos == newItems.size() ? Q_NULLPTR : newItems.at(new_pos);
            ParamItem *nextItem = item_pos == this->paramItemsCount() ? Q_NULLPTR : this->paramItem(item_pos);

            if (!nextItem && !nextNewRec) {
                stat = StateNone;
            } else if (!nextItem) {
                stat = StateAppend;
            } else if (!nextNewRec) {
                stat = StateRemove;
            } else if (paramUpdateItemIsEqual(nextItem, nextNewRec)) {
                stat = StateChanged;
            } else if (paramUpdateItemIsGreater(nextItem, nextNewRec)) {
                stat = StateAppend;
            } else {
                stat = StateRemove;
            }

            if (stat != prev_stat) {
                if (prev_stat == StateAppend) {
                    model->paramItemsBeginInsert(item_pos, item_pos + rep_cnt);
                    for (int i = 0; i <= rep_cnt; i++) {
                        this->paramItemInsert(item_pos++, paramUpdateCreateItem(newItems.at(new_pos - (rep_cnt + 1) + i)));
                    }
                    model->paramItemsEndInsert();
                } else if (prev_stat == StateRemove) {
                    item_pos -= (rep_cnt + 1);
                    model->paramItemsBeginRemove(item_pos, item_pos + rep_cnt);
                    for (int i = 0; i <= rep_cnt; i++) {
                        paramUpdateRemItem(item_pos);
                    }
                    model->paramItemsEndRemove();
                } else if (prev_stat == StateChanged) {
                    for (int i = 0; i <= rep_cnt; i++) {
                        this->paramUpdateItemData(this->paramItem(item_pos - (rep_cnt + 1) + i), newItems.at(new_pos - (rep_cnt + 1) + i));
                    }
                    model->paramItemsUpdate(item_pos - (rep_cnt + 1), item_pos - 1);
                }
                prev_stat = stat;
                rep_cnt = 0;
            } else {
                rep_cnt++;
            }

            if (stat == StateAppend) {
                new_pos++;
            } else if (stat == StateRemove) {
                item_pos++;
            } else if (stat == StateChanged) {
                item_pos++;
                new_pos++;
            }
        } while (stat != StateNone);
    }


};


#endif // TABLEPARAMSUPDATEDMODEL_H
