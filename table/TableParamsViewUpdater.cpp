#include "TableParamsViewUpdater.h"
#include "TableParamsModelIface.h"
#include "actions/TableCopyAction.h"
#include <QTableView>
#include <QHeaderView>
#include <QSettings>
#include <QEvent>


TableParamsViewUpdater::TableParamsViewUpdater(QTableView *view,
                                               TableParamsModelIface *modelIface,
                                               QAbstractItemModel *model) :
    QObject{view}, m_view{view}, m_modelIface{modelIface},
    m_model{model ? model : modelIface->paramItemModel()} {

    m_view->setModel(m_model);
    initRows();
    connect(m_model, &QAbstractItemModel::columnsInserted,
            this, &TableParamsViewUpdater::onParamsInserted);
    connect(m_model, &QAbstractItemModel::modelAboutToBeReset,
            this, &TableParamsViewUpdater::preModelReset);
    connect(m_model, &QAbstractItemModel::modelReset,
            this, &TableParamsViewUpdater::onModelReset);
    m_view->installEventFilter(this);
}

void TableParamsViewUpdater::loadState(const QSettings &set, const QString &key) {
    m_name = key;
    m_setgroup = set.group();
    reloadState(set, key);
}

void TableParamsViewUpdater::reloadState(const QSettings &set, const QString &key) {
    TableViewSettings::instance()->restoreColumnsState(
                m_view->horizontalHeader(), set, key);
}

void TableParamsViewUpdater::reloadState() {
    if (!m_name.isEmpty()) {
        QSettings set;
        set.beginGroup(m_setgroup);
        reloadState(set, m_name);
        set.endGroup();
    }
}

void TableParamsViewUpdater::saveState() const {
    if (!m_name.isEmpty()) {
        QSettings set;
        set.beginGroup(m_setgroup);
        saveState(set, m_name);
        set.endGroup();
    }
}

void TableParamsViewUpdater::saveState(QSettings &set, const QString &key) const {
    QHeaderView *hdr = m_view->horizontalHeader();
    int params_cnt  = hdr->count();
    if (params_cnt > 0)
        TableViewSettings::instance()->saveColumnsState(hdr, set, key);
}

void TableParamsViewUpdater::addCopyAction() {
    TableCopyAction *copy_act = new TableCopyAction(m_view);
    connect(this, &TableParamsViewUpdater::languageChanged, copy_act, &TableCopyAction::retranslate);
    m_view->addAction(copy_act);
}

void TableParamsViewUpdater::onParamsInserted(const QModelIndex &parent, int first, int last) {
    Q_UNUSED(parent)
    initRows(first, last);
}

void TableParamsViewUpdater::preModelReset() {
    if (!m_name.isEmpty())
        saveState();
}

void TableParamsViewUpdater::onModelReset() {
    if (!m_name.isEmpty()) {
        initRows();
        reloadState();
    }
}

bool TableParamsViewUpdater::eventFilter(QObject *obj, QEvent *event) {
    if (event->type() == QEvent::LanguageChange) {
        Q_EMIT languageChanged();
    }
    return QObject::eventFilter(obj, event);
}

void TableParamsViewUpdater::initRows(int from, int to) {
    if (to < 0)
        to = m_model->columnCount()-1;
    for (int col {from}; col <= to; col++) {
        QAbstractItemDelegate *deletgate = m_modelIface->paramCreateDelegate(col, m_view);
        int width = m_modelIface->paramDefaultColumnWidth(col);
        if (deletgate) {
            m_view->setItemDelegateForColumn(col, deletgate);
        }
        if (width > 0)
            m_view->setColumnWidth(col, width);
    }
}
