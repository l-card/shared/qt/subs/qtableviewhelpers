#ifndef TABLEAUTOSCROLLER_H
#define TABLEAUTOSCROLLER_H

#include <QObject>

class QTableView;


class TableAutoScrollMode {
public:
    virtual ~TableAutoScrollMode();
    virtual bool required(QTableView &view) const = 0;
    virtual void execute(QTableView &view) const = 0;
};


class TableAutoScroller : public QObject {
    Q_OBJECT
public:


    /* Режим в котором если полоса прокрутки находится внизу, то автоматически
     * происходит прокрутка вниз при добавлении следующей строки */
    static const TableAutoScrollMode &scrollMode();
    /* Режим в котором если текущая ячейка находится в последней строке, то
     * при добавлении новой строки ее ячейка делается текущей и происходит проерутка
     * до этой строки. Также если выбраны ячейки, то выбор сдвигается на строку ниже */
    static const TableAutoScrollMode &currentRowMode();

    TableAutoScroller(QTableView *table, const TableAutoScrollMode &mode = scrollMode());
public:
    void setAutoScrollMode(const TableAutoScrollMode &mode);

private Q_SLOTS:
    void checkScroll();
    void scrollExec();
private:
    QTableView *m_table;
    const TableAutoScrollMode *m_mode;

    bool m_autoscroll_on;
};

#endif // TABLEAUTOSCROLLER_H
