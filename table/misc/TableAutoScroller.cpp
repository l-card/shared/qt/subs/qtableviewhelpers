#include "TableAutoScroller.h"
#include <QTableView>
#include <QScrollBar>
#include <QAbstractItemModel>

const TableAutoScrollMode &TableAutoScroller::scrollMode() {
    static const class TableAutoScrollModeScroll : public TableAutoScrollMode {
        bool required(QTableView &view) const override {
            QScrollBar* bar = view.verticalScrollBar();
            return bar->value() == bar->maximum();
        }
        void execute(QTableView &view) const override {
            view.scrollToBottom();
        }
    } mode;
    return mode;
}

const TableAutoScrollMode &TableAutoScroller::currentRowMode() {
    static const class TableAutoScrollModeCurrentRow : public TableAutoScrollMode {
        bool required(QTableView &view) const override {
            int rowCount = view.model()->rowCount();
            int curRow = view.currentIndex().row();
            return (curRow < 0) || (curRow == (rowCount - 1));
        }
        void execute(QTableView &view) const override {
            int rowCount = view.model()->rowCount();


            /* изменяем строку текущего индекса на последний. Если не было, то берем первый столбец */
            QModelIndex curIdx = view.currentIndex();
            QModelIndex idx = view.model()->index(rowCount - 1, curIdx.isValid() ? curIdx.column() : 0);
            view.selectionModel()->setCurrentIndex(idx, QItemSelectionModel::NoUpdate);

            /* если были выбраны ячейки, сдвигаем выбор также вниз на одну строку */
            QModelIndexList selList = view.selectionModel()->selectedIndexes();
            if (!selList.isEmpty()) {
                QItemSelection newSel;
                Q_FOREACH(QModelIndex idx, selList) {
                    QModelIndex newIdx = view.model()->index(idx.row() + 1, idx.column());
                    newSel.select(newIdx, newIdx);
                }
                view.selectionModel()->select(newSel, QItemSelectionModel::ClearAndSelect);
            }
        }
    } mode;
    return mode;
}

TableAutoScroller::TableAutoScroller(QTableView *table, const TableAutoScrollMode &mode) :
    QObject (table), m_table(table), m_mode(&mode), m_autoscroll_on(false) {

    connect(table->model(), &QAbstractItemModel::rowsAboutToBeInserted, this, &TableAutoScroller::checkScroll);
    checkScroll();
}


void TableAutoScroller::setAutoScrollMode(const TableAutoScrollMode &mode) {
    m_mode = &mode;
    checkScroll();
}

void TableAutoScroller::checkScroll() {
    bool required = m_mode && m_mode->required(*m_table);
    if (required != m_autoscroll_on) {
        if (required) {
           connect(m_table->model(), &QAbstractItemModel::rowsInserted,
                   this, &TableAutoScroller::scrollExec);
        } else {
            disconnect(m_table->model(), &QAbstractItemModel::rowsInserted,
                    this, &TableAutoScroller::scrollExec);
        }
        m_autoscroll_on = required;
    }
}

void TableAutoScroller::scrollExec() {
    if (m_mode)
        m_mode->execute(*m_table);
}


TableAutoScrollMode::~TableAutoScrollMode() {

}
