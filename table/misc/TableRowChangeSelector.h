#ifndef TABLEROWCHANGESELECTOR_H
#define TABLEROWCHANGESELECTOR_H

#include <QObject>
class QTableView;

/* объект при изменении строк в модели изменяет выбор строки в таблице таким образом,
 * чтобы была выделенна измененная строка.
 * Обрабатываются:
 *  - при добавлениии - выделяется первая добавленная
 *  - при удалении - строка за удаленной
 *  - при перемещении - первая перемещенная
 *  - при сбросе модели - первая в списке
 * модель уже должна быть установлена в QTableView на момент создания объекта */
class TableRowChangeSelector : public QObject {
    Q_OBJECT
public:
    TableRowChangeSelector(QTableView *table);

private Q_SLOTS:
    void onRowInserted(const QModelIndex &parent, int first, int last);
    void onRowRemoved(const QModelIndex &parent, int first, int last);
    void onRowMoved(const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);
    void onReset();

    void selectRow(int row);

private:
    QTableView *m_table;
};

#endif // TABLEROWCHANGESELECTOR_H
