#include "TableHeightUpdater.h"
#include <QTableView>
#include <QScrollBar>
#include <QHeaderView>
#include <QEvent>

TableHeightUpdater::TableHeightUpdater(QTableView *table, Flags flags) :
    QObject(table), m_table(table), m_flags(flags), m_last_height(0) {

    table->horizontalScrollBar()->installEventFilter(this);
    connect(table->model(), &QAbstractItemModel::rowsInserted, this, &TableHeightUpdater::updateHeight);
    connect(table->model(), &QAbstractItemModel::rowsRemoved, this, &TableHeightUpdater::updateHeight);
    connect(table->model(), &QAbstractItemModel::modelReset, this, &TableHeightUpdater::updateHeight);
    connect(table, &QTableView::destroyed, this, &TableHeightUpdater::onTableRemove);
    updateHeight();
}

int TableHeightUpdater::setTableHeight(QTableView &table, Flags flags) {
    int height = 0;
    if (flags & UpdateRowByContent)
        table.resizeRowsToContents();

    height = table.horizontalHeader()->height();
    for (int i=0; i < table.model()->rowCount(); i++) {
        height += table.rowHeight(i);
    }
    if (table.horizontalScrollBar()->isVisible())
        height += table.horizontalScrollBar()->height();

    height += 2;
    table.setMaximumHeight(height);
    if (flags & FixedHight) {
        table.setMinimumHeight(height);
        table.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }
    return height;
}

bool TableHeightUpdater::eventFilter(QObject *obj, QEvent *event) {
    /* необходимо заново подбирать размер таблицы при появлении и удалинии
     * горизонтальной прокрутки */
    if ((event->type() == QEvent::Show) || (event->type() == QEvent::Hide)) {
        updateHeight();
    }
    return QObject::eventFilter(obj, event);
}

void TableHeightUpdater::updateHeight() {
    if (m_table) {
        int h = setTableHeight(*m_table, m_flags);
        if (h != m_last_height) {
            m_last_height = h;
            Q_EMIT heightChanged(h);
        }
    }
}

void TableHeightUpdater::onTableRemove() {
    m_table = Q_NULLPTR;
}
