#ifndef TABLEHIGHTUPDATER_H
#define TABLEHIGHTUPDATER_H

#include <QObject>
class QTableView;

/* Класс устанавливает максимальную высоту таблицы по высоте ее строк и отслеживает
 * изменения в таблице, влияющие на размер (появление горизонтальной прокрутки
 * или изменения кол-ва строк).
 *  */
class TableHeightUpdater : public QObject {
    Q_OBJECT
public:
    enum Flag {
        NoFlags             = 0,
        FixedHight          = 1 << 0, /* Минимальный размер таблицы также устанавливается
                                             по высоте столбцов и запрещается верстикальная прокрутка */
        UpdateRowByContent  = 1 << 1  /* Перед расчетом высоты выполнить подгонгу высоты строк
                                             через resizeRowsToContents() */
    };
    Q_DECLARE_FLAGS(Flags, Flag);

    TableHeightUpdater(QTableView *table, Flags flags = NoFlags);
    static int setTableHeight(QTableView &table, Flags flags);

    int tableHeight() const {return m_last_height;}
public Q_SLOTS:
    void updateHeight();
Q_SIGNALS:
    void heightChanged(int height);
protected:
    bool eventFilter(QObject *obj, QEvent *event);
private Q_SLOTS:
    void onTableRemove();
private:
    QTableView *m_table;
    Flags m_flags;
    int m_last_height;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(TableHeightUpdater::Flags)


#endif // TABLEHIGHTUPDATER_H
