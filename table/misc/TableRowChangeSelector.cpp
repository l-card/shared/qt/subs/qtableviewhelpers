#include "TableRowChangeSelector.h"
#include <QTableView>

TableRowChangeSelector::TableRowChangeSelector(QTableView *table) :
    QObject (table), m_table(table) {

    connect(table->model(), &QAbstractItemModel::rowsInserted, this, &TableRowChangeSelector::onRowInserted);
    connect(table->model(), &QAbstractItemModel::rowsRemoved, this, &TableRowChangeSelector::onRowRemoved);
    connect(table->model(), &QAbstractItemModel::rowsMoved, this, &TableRowChangeSelector::onRowMoved);
    connect(table->model(), &QAbstractItemModel::modelReset, this, &TableRowChangeSelector::onReset);
}

void TableRowChangeSelector::onRowInserted(const QModelIndex &parent, int first, int last) {
    selectRow(first);
}

void TableRowChangeSelector::onRowRemoved(const QModelIndex &parent, int first, int last) {
    int rows_cnt = m_table->model()->rowCount();
    if (rows_cnt > 0) {
        selectRow(qMin(first, (rows_cnt-1)));
    }
}

void TableRowChangeSelector::onRowMoved(const QModelIndex &parent, int start,
                                        int end, const QModelIndex &destination, int row) {
    selectRow(row > start ? row - 1 : row);
}

void TableRowChangeSelector::onReset() {
    int rows_cnt = m_table->model()->rowCount();
    if (rows_cnt > 0) {
        selectRow(0);
    }
}

void TableRowChangeSelector::selectRow(int row) {
    m_table->selectRow(row);
    m_table->setFocus();
}
