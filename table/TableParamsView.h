#ifndef TABLEPARAMSVIEW_H
#define TABLEPARAMSVIEW_H

#include <QTableView>
#include "misc/TableHeightUpdater.h"
class TableParamsViewUpdater;
class TableParamsModelIface;
class TableAutoScrollMode;
class QAbstractItemModel;
class QSettings;

/* Вспомогательный класс, определяющий таблицу, включающую
 * всю функциональность из TableParamsViewUpdater */
class TableParamsView : public QTableView {
    Q_OBJECT
public:
    explicit TableParamsView(QWidget *parent = nullptr);

    int selectedRow() const;


    void init(TableParamsModelIface *modelIface, QAbstractItemModel *model = nullptr);
    void loadState(const QSettings &set, const QString &key);
    void saveState(QSettings &set, const QString &key) const;
    void saveState() const;
    void reloadState(const QSettings &set, const QString &key);
    void reloadState();
    void addCopyAction();

    void setReadOnly(bool en);

    void enableHeightAutoUpdate(TableHeightUpdater::Flags flags = TableHeightUpdater::NoFlags);
    void enableAutoScroll();
    void enableAutoScroll(const TableAutoScrollMode &mode);
    void enableChangedRowAutoSelect();

    void highUpdate();
public Q_SLOTS:
    /* аналог слота QTableView::update, но с уникальным именем, чтобы можно было подключать к сигналам */
    void updateIndex(const QModelIndex &idx);
private:
    TableParamsViewUpdater *m_updater {nullptr};
    TableHeightUpdater *m_high_updater {nullptr};
};

#endif // TABLEPARAMSVIEW_H
