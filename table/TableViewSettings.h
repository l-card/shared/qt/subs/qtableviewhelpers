#ifndef TABLEVIEWSETTINGS_H
#define TABLEVIEWSETTINGS_H

#include <QBrush>
#include <QObject>

class QTableView;
class QHeaderView;
class QSettings;

class TableViewSettings : public QObject {
    Q_OBJECT
public:
    static TableViewSettings *instance();


    enum InitFlag {
        InitNoFlags  = 0x00,
        InitNoAction = 0x01,
    };
    Q_DECLARE_FLAGS(InitFlags, InitFlag)


    QBrush disabledTableElementBrush() const {return m_disabledTableElemBrush;}
    QBrush readonlyTableElementBrush() const;
    QBrush notAssignedTableElementBrush() const;
    QBrush positiveTableElementBrush() const {return m_positiveTableElemBrush;}
    QBrush negativeTableElementBrush() const {return m_negativeTableElemBrush;}
    QBrush progressTableElementBrush() const {return m_progressTableElemBrush;}

    void initDataView(QTableView *view, QString name, const QSettings &set, InitFlags flags = InitNoFlags) const;
    void saveDataView(const QTableView *view, QSettings &set) const;

    static void restoreColumnsState(QHeaderView *view, const QSettings &set, const QString &name);
    static void saveColumnsState(const QHeaderView *view, QSettings &set, const QString &name);

public Q_SLOTS:
    void retranslate();
Q_SIGNALS:
    void languageChanged();
private:
    TableViewSettings();

    QBrush m_disabledTableElemBrush;
    QBrush m_positiveTableElemBrush;
    QBrush m_negativeTableElemBrush;
    QBrush m_progressTableElemBrush;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(TableViewSettings::InitFlags)

#endif // TABLEVIEWSETTINGS_H
