#ifndef BOOLBOXEDITORDELEGATE_H
#define BOOLBOXEDITORDELEGATE_H

#include "ComboBoxEditorDelegate.h"

class BoolBoxEditorDelegate : public ComboBoxEditorDelegate {
    Q_OBJECT
public:
    explicit BoolBoxEditorDelegate(QObject *parent = nullptr);


    static QString defaultValText(bool val) {return val ? tr("Yes") : tr("No");}
    virtual QString valueText(bool val) const {return defaultValText(val);}
protected:
    void fillBoxItems(QComboBox *box, const QModelIndex &index) const override;
    QVariant getBoxData(QComboBox *box, int idx) const override;
    int getBoxIndexByData(QComboBox *box, const QVariant &data) const override;
};

#endif // BOOLBOXEDITORDELEGATE_H
