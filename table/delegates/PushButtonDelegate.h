#ifndef PUSHBUTTONDELEGATE_H
#define PUSHBUTTONDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>
class TableParamsView;
class PushButtonDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    explicit PushButtonDelegate(const QString &text, QObject *parent = nullptr);

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override {
        Q_UNUSED(parent) Q_UNUSED(option) Q_UNUSED(index)
        return nullptr;
    }

    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;
Q_SIGNALS:
    void updateRequest(const QModelIndex &index);
    void clicked(const QModelIndex &index);
protected:
    bool eventFilter(QObject *obj, QEvent *event) override;
private:
    void leaveFocus();

    QString m_text;
    QModelIndex m_lastMouseActiveIdx;
    bool m_isPressed {false};
    TableParamsView *m_tableView;
};

#endif // PUSHBUTTONDELEGATE_H
