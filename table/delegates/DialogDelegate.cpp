#include "DialogDelegate.h"
#include <QDialog>
#include <QEvent>

DialogDelegate::DialogDelegate(QObject *parent) :
    BaseType{parent},
    m_topWgt{qobject_cast<QWidget *>(parent)} {

    while (m_topWgt && m_topWgt->parentWidget()) {
        m_topWgt = m_topWgt->parentWidget();
    }
}

DialogDelegate::~DialogDelegate() {
    if (m_dlg) {
        m_dlg->close();
        m_dlg->deleteLater();
    }
}

bool DialogDelegate::editorEvent(QEvent *event, QAbstractItemModel *model,
                                 const QStyleOptionViewItem &option, const
                                 QModelIndex &index) {
    if (!m_dlg) {
        if ((event->type() == QEvent::Type::MouseButtonDblClick) ||
                (event->type() == QEvent::Type::KeyPress)) {
            m_dlg = createDialog(m_topWgt, model, index);
            if (m_dlg) {
                m_model = model;
                m_index = index;
                m_dlg->setModal(true);
                m_dlg->show();
                connect(m_dlg, &QDialog::finished, this, &DialogDelegate::onDialogFinished);
            }
        }
    }
    return BaseType::editorEvent(event, model, option, index);
}

QWidget *DialogDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const {
    Q_UNUSED(parent) Q_UNUSED(option) Q_UNUSED(index)
    return nullptr;
}

void DialogDelegate::onDialogFinished(int result) {
    if (m_dlg) {
        if (result == QDialog::DialogCode::Accepted) {
            saveDialogData(m_dlg, m_model, m_index);
        }
        m_dlg->deleteLater();
        m_dlg = nullptr;
    }
}
