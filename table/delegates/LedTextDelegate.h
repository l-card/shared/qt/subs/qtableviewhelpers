#ifndef LEDTEXTDELEGATE_H
#define LEDTEXTDELEGATE_H

#include "LedDelegate.h"

class LedTextDelegate : public LedDelegate {
public:
    explicit LedTextDelegate(QObject *parent = nullptr, DelegateIndexSelector *paintSelector = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // LEDTEXTDELEGATE_H
