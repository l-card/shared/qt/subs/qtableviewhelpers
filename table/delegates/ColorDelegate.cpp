#include "ColorDelegate.h"
#include <QColorDialog>
#include <QBrush>
#include <QPainter>


ColorDelegate::ColorDelegate(QObject *parent) : DialogDelegate{parent} {

}

QDialog *ColorDelegate::createDialog(QWidget *parent, QAbstractItemModel *model, const QModelIndex &index) {
    QColorDialog *dlg{new QColorDialog(parent)};
    dlg->setCurrentColor(QColor(index.data(Qt::ItemDataRole::EditRole).toString()));
    return dlg;
}

void ColorDelegate::saveDialogData(QDialog *dialog, QAbstractItemModel *model, const QModelIndex &index) {
    QColorDialog *colorDlg {qobject_cast<QColorDialog *>(dialog)};
    if (colorDlg) {
        model->setData(index, colorDlg->currentColor().name(), Qt::EditRole);
    }
}

void ColorDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QBrush bgBrush {QColor{index.data(Qt::ItemDataRole::EditRole).toString()}};
    painter->fillRect(option.rect, bgBrush);
}
