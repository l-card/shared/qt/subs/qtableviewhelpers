#ifndef COMBOBOXEDITORDELEGATE_H
#define COMBOBOXEDITORDELEGATE_H


#include <QStyledItemDelegate>


class QComboBox;


class ComboBoxEditorDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    static const int ItemsTextListRole {static_cast<int>(Qt::ItemDataRole::UserRole)};

    explicit ComboBoxEditorDelegate(QObject *parent = nullptr, bool editable = false);
    ~ComboBoxEditorDelegate() override;


    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    bool eventFilter(QObject *object, QEvent *event) override;
protected:
    virtual void fillBoxItems(QComboBox *box, const QModelIndex &index) const;
    virtual QVariant getBoxData(QComboBox *box, int idx) const;    
    virtual QVariant getModelBoxData(const QModelIndex &index) const;
    virtual int getBoxIndexByData(QComboBox *box, const QVariant &data) const;

private Q_SLOTS:
    void onItemActivated(int idx);

private:
    mutable QComboBox *m_box {nullptr};
    mutable int m_idx {-1};
    bool m_editable;

};

#endif // COMBOBOXEDITORDELEGATE_H
