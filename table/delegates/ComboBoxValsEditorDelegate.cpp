#include "ComboBoxValsEditorDelegate.h"
#include <QComboBox>

ComboBoxValsEditorDelegate::ComboBoxValsEditorDelegate(QObject *parent) :
    ComboBoxEditorDelegate{parent, false} {

}

void ComboBoxValsEditorDelegate::fillBoxItems(QComboBox *box, const QModelIndex &index) const {
    const auto &items{index.data(ItemsValuesListRole).value<QList<ComboBoxValsEditorDelegate::Item> >()};
    for (const ComboBoxValsEditorDelegate::Item &item : items) {
        box->addItem(item.name, item.value);
    }
}

int ComboBoxValsEditorDelegate::getBoxIndexByData(QComboBox *box, const QVariant &data) const {
    int ret {-1};
    ComboBoxValsEditorDelegate::Item item{data.value<ComboBoxValsEditorDelegate::Item>()};
    for (int i {0}; (i < box->count()) && (ret < 0); ++i) {
        if (box->itemData(i) == item.value) {
           ret = i;
        }
    }
    return ret;
}

QVariant ComboBoxValsEditorDelegate::getBoxData(QComboBox *box, int idx) const {
    return QVariant::fromValue(ComboBoxValsEditorDelegate::Item{box->currentText(), box->currentData()});
}
