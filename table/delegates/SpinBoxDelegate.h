#ifndef SPINBOXDELEGATE_H
#define SPINBOXDELEGATE_H

#include <QStyledItemDelegate>

class SpinBoxDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    static const int StepRole               { Qt::ItemDataRole::UserRole + 1 };
    static const int MaxRole                { Qt::ItemDataRole::UserRole + 2 };
    static const int MinRole                { Qt::ItemDataRole::UserRole + 3 };
    static const int SpecialValueTextRole   { Qt::ItemDataRole::UserRole + 4 };
    static const int PrefixTextRole         { Qt::ItemDataRole::UserRole + 5 };
    static const int SuffixTextRole         { Qt::ItemDataRole::UserRole + 6 };

    explicit SpinBoxDelegate(QObject *parent = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // SPINBOXDELEGATE_H
