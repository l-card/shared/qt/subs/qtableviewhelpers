#ifndef LEDDELEGATE_H
#define LEDDELEGATE_H

#include <QStyledItemDelegate>
#include <memory>

class DelegateIndexSelector;

class LedDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    static const int Color1Role { Qt::ItemDataRole::UserRole + 1 };
    static const int Color2Role { Qt::ItemDataRole::UserRole + 2 };


    explicit LedDelegate(QObject *parent = nullptr, DelegateIndexSelector *paintSelector = nullptr);
    ~LedDelegate() override;

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override {
        Q_UNUSED(parent) Q_UNUSED(option) Q_UNUSED(index)
        return nullptr;
    }

protected:
    bool paintEnabled(const QModelIndex &index) const;
    void paintLed(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QModelIndex &index) const;
private:
    const std::unique_ptr<DelegateIndexSelector> m_paintSelector;
};

#endif // LEDDELEGATE_H
