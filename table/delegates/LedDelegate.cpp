#include "LedDelegate.h"
#include <QColor>
#include <QPainter>
#include "DelegateIndexSelector.h"

LedDelegate::LedDelegate(QObject *parent, DelegateIndexSelector *paintSelector) :
    QStyledItemDelegate{parent}, m_paintSelector{paintSelector} {

}

LedDelegate::~LedDelegate() {

}

void LedDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    if (!paintEnabled(index)) {
        QStyledItemDelegate::paint(painter, option, index);
    } else {
        paintLed(painter, option, option.rect, index);
    }
}

bool LedDelegate::paintEnabled(const QModelIndex &index) const {
    return !m_paintSelector || m_paintSelector->delegatePaintEnabled(index);
}

void LedDelegate::paintLed(QPainter *painter, const QStyleOptionViewItem &option, const QRect &rect, const QModelIndex &index) const {
    static const qreal scaledSize {1000};
    const QColor color1 { index.data(Color1Role).value<QColor>() };
    const QColor color2 { index.data(Color2Role).value<QColor>() };
    const int check     { index.data(Qt::ItemDataRole::CheckStateRole).toInt() };

    painter->save();

    const QVariant bgBrush {index.data(Qt::ItemDataRole::BackgroundRole)};
    if (bgBrush.isValid()) {
        painter->fillRect(rect, bgBrush.value<QBrush>());
    } else if (option.state & QStyle::StateFlag::State_Selected) {
        painter->fillRect(rect, option.palette.highlight());
    }


    const qreal width  { static_cast<qreal>(rect.width() - 4) };
    const qreal height { static_cast<qreal>(rect.height() - 4) };
    const qreal x { static_cast<qreal>(rect.x() + 2) };
    const qreal y { static_cast<qreal>(rect.y() + 2) };

    const qreal realSize {qMin(width, height)};

    QRadialGradient gradient;
    QPen pen{Qt::GlobalColor::black};
    pen.setWidth(1);

    painter->setRenderHint(QPainter::RenderHint::Antialiasing);
    painter->translate(x + width/2, y + height/2);
    painter->scale(realSize/scaledSize, realSize/scaledSize);

    gradient = QRadialGradient{QPointF{-500, -500}, 1500, QPointF{-500, -500}};
    gradient.setColorAt(0, QColor{224,224,224});
    gradient.setColorAt(1, QColor{ 28, 28, 28});
    painter->setPen(pen);
    painter->setBrush(QBrush{gradient});
    painter->drawEllipse(QPointF{0,0}, 500, 500);

    gradient = QRadialGradient{QPointF{500, 500}, 1500, QPointF{500, 500}};
    gradient.setColorAt(0, QColor{224,224,224});
    gradient.setColorAt(1, QColor{ 28, 28, 28});
    painter->setPen(pen);
    painter->setBrush(QBrush{gradient});
    painter->drawEllipse(QPointF{0,0}, 450, 450);

    painter->setPen(pen);
    if (check == Qt::CheckState::Checked) {
        gradient = QRadialGradient{QPointF{-500, -500}, 1500, QPointF{-500, -500}};
    } else {
        gradient = QRadialGradient{QPointF{500, 500}, 1500, QPointF{500, 500}};
    }
    gradient = QRadialGradient{QPointF{-500, -500}, 1500, QPointF{-500, -500}};
    gradient.setColorAt(0, color1);
    gradient.setColorAt(1, color2);

    painter->setBrush(gradient);
    painter->drawEllipse(QPointF{0,0}, 400, 400);

    painter->restore();
}
