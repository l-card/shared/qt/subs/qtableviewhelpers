#include "SpinBoxDelegate.h"
#include <QSpinBox>

SpinBoxDelegate::SpinBoxDelegate(QObject *parent) : QStyledItemDelegate{parent} {

}

QWidget *SpinBoxDelegate::createEditor(QWidget *parent,
                                       const QStyleOptionViewItem &option,
                                       const QModelIndex &index) const {
    Q_UNUSED(option)
    QSpinBox *editor {new QSpinBox{parent}};
    editor->setFrame(false);

    QVariant varval;

    int step  {1};
    int max_val {9999};
    int min_val {-9999};
    QString specValueText;
    QString prefixText;
    QString suffixText;

    varval = index.model()->data(index, StepRole);
    if (varval.isValid()) {
        step = varval.toInt();
    }

    varval = index.model()->data(index, MaxRole);
    if (varval.isValid()) {
        max_val = varval.toInt();
    }

    varval = index.model()->data(index, MinRole);
    if (varval.isValid()) {
        min_val = varval.toInt();
    }

    varval = index.model()->data(index, SpecialValueTextRole);
    if (varval.isValid()) {
        specValueText = varval.toString();
    }

    varval = index.model()->data(index, PrefixTextRole);
    if (varval.isValid()) {
        prefixText = varval.toString();
    }

    varval = index.model()->data(index, SuffixTextRole);
    if (varval.isValid()) {
        suffixText = varval.toString();
    }

    editor->setSingleStep(step);
    editor->setMaximum(max_val);
    editor->setMinimum(min_val);
    editor->setSpecialValueText(specValueText);
    editor->setPrefix(prefixText);
    editor->setSuffix(suffixText);
    return editor;
}

void SpinBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    const int value {index.model()->data(index, Qt::ItemDataRole::EditRole).toInt()};
    QSpinBox *spinBox {static_cast<QSpinBox*>(editor)};

    spinBox->setValue(value);
}

void SpinBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const {
    QSpinBox *spinBox {static_cast<QSpinBox*>(editor)};
    spinBox->interpretText();
    int value = spinBox->value();

    model->setData(index, value, Qt::EditRole);
}

void SpinBoxDelegate::updateEditorGeometry(QWidget *editor,
                                           const QStyleOptionViewItem &option,
                                           const QModelIndex &index) const {
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}
