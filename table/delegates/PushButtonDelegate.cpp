#include "PushButtonDelegate.h"
#include "../TableParamsView.h"
#include <QApplication>
#include <QMouseEvent>

PushButtonDelegate::PushButtonDelegate(const QString &text, QObject *parent) :
    QStyledItemDelegate{parent},
    m_text{text},
    m_tableView{qobject_cast<TableParamsView *>(parent)} {

    /* для случая, когда paretn - TableParamsView, делаем необходимые подключения
     * по обновлению ячейки автоматом.
     * Иначе необходимо самостоятельно вызывать View::update по сигналу updateRequest(),
     * а также отслеживать выход c viewport */
    if (m_tableView) {
        connect(this, &PushButtonDelegate::updateRequest, m_tableView, &TableParamsView::updateIndex);
        m_tableView->viewport()->installEventFilter(this);
    }
}

void PushButtonDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyledItemDelegate::paint(painter, option, index);

    QStyleOptionButton opt;

    if (index == m_lastMouseActiveIdx) {
        opt.state |= QStyle::State_MouseOver;
        if (m_isPressed) {
            opt.state |= QStyle::State_Sunken;
        }
    }

    opt.state |= QStyle::State_Enabled;
    opt.rect = option.rect.adjusted(1, 1, -1, -1);
    opt.text = m_text;
    QApplication::style()->drawControl(QStyle::CE_PushButton, &opt, painter, 0);
}

bool PushButtonDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) {
    if (event->type() == QEvent::MouseMove) {
        if (index != m_lastMouseActiveIdx) {
            m_isPressed = false;
            const QModelIndex prevActive {m_lastMouseActiveIdx};
            m_lastMouseActiveIdx = index;
            if (prevActive.isValid()) {
                Q_EMIT updateRequest(prevActive);
            }
            if (m_lastMouseActiveIdx.isValid()) {
                Q_EMIT updateRequest(m_lastMouseActiveIdx);
            }
        }
    } else if ((event->type() == QEvent::MouseButtonPress)
               || (event->type() == QEvent::MouseButtonDblClick)) {
        if (index != m_lastMouseActiveIdx) {
            const QModelIndex prevActive {m_lastMouseActiveIdx};
            m_lastMouseActiveIdx = index;
            if (prevActive.isValid()) {
                Q_EMIT updateRequest(prevActive);
            }
        }

        if (m_lastMouseActiveIdx.isValid()) {
            m_isPressed = true;
            Q_EMIT updateRequest(m_lastMouseActiveIdx);
            Q_EMIT clicked(m_lastMouseActiveIdx);
        }
    } else if (event->type() == QEvent::MouseButtonRelease) {
        m_isPressed = false;
        if (index != m_lastMouseActiveIdx) {
            const QModelIndex prevActive {m_lastMouseActiveIdx};
            m_lastMouseActiveIdx = index;
            if (prevActive.isValid()) {
                Q_EMIT updateRequest(prevActive);
            }
        }

        if (m_lastMouseActiveIdx.isValid()) {
            Q_EMIT updateRequest(m_lastMouseActiveIdx);
        }
    } else if (event->type() == QEvent::Leave) {
        leaveFocus();
    }
    return QStyledItemDelegate::editorEvent(event, model, option, index);
}

bool PushButtonDelegate::eventFilter(QObject *obj, QEvent *event) {
    /* обслуживаем события от viewport, чтобы отслелить как выход за область
     * действительных ячеек таблицы, так и за область самого viewport
     * (события делегату могут не приходить) */
    if (event->type() == QEvent::Leave) {
        leaveFocus();
    } else if (event->type() == QEvent::MouseMove) {
        if (m_tableView) {
            const QModelIndex index {m_tableView->indexAt(static_cast<QMouseEvent *>(event)->pos())};
            if (!index.isValid())
                leaveFocus();
        }
    }
    return QStyledItemDelegate::eventFilter(obj, event);
}

void PushButtonDelegate::leaveFocus() {
    m_isPressed = false;
    if (m_lastMouseActiveIdx.isValid()) {
        const QModelIndex prevActive {m_lastMouseActiveIdx};
        m_lastMouseActiveIdx = QModelIndex{};
        Q_EMIT updateRequest(prevActive);
    }
}
