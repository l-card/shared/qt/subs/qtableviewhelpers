#ifndef CHECKBOXITEMDELEGATE_H
#define CHECKBOXITEMDELEGATE_H


#include <QStyledItemDelegate>
#include <QModelIndex>
#include <memory>

class DelegateIndexSelector;

class CheckBoxItemDelegate : public QStyledItemDelegate  {
    Q_OBJECT
    using BaseType = QStyledItemDelegate;
public:
    explicit CheckBoxItemDelegate(QObject *parent, Qt::AlignmentFlag alignment = Qt::AlignmentFlag::AlignHCenter,
                                  DelegateIndexSelector *paintSelector = nullptr);
    ~CheckBoxItemDelegate() override;

    void paint(QPainter *painter, const QStyleOptionViewItem &option_init,
               const QModelIndex &index) const override;
    bool editorEvent( QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option,
                      const QModelIndex &index ) override;

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override {
        Q_UNUSED(parent);
        Q_UNUSED(option);
        Q_UNUSED(index);
        return Q_NULLPTR;
    }
private:
    void fillCheckRect(const QStyleOptionViewItem &option, QStyleOptionButton &btnOpt) const;

    QModelIndex m_lastClickedIndex;
    std::unique_ptr<DelegateIndexSelector> m_paintSelector;
    Qt::AlignmentFlag m_align;
};


#endif // CHECKBOXITEMDELEGATE_H
