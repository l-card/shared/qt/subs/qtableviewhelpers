#include "ComboBoxEditorDelegate.h"
#include <QComboBox>
#include <QEvent>
#include <QKeyEvent>
#include <QAbstractItemView>

ComboBoxEditorDelegate::ComboBoxEditorDelegate(QObject *parent, bool editable) :
    QStyledItemDelegate{parent},
    m_editable{editable} {
}

ComboBoxEditorDelegate::~ComboBoxEditorDelegate() {

}

QWidget *ComboBoxEditorDelegate::createEditor(
        QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    Q_UNUSED(option)
    m_idx = -1;

    QComboBox *box {new QComboBox{parent}};
    box->setFrame(false);
    fillBoxItems(box, index);
    box->setAutoFillBackground(true);
    box->setEditable(m_editable);
    connect(box, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
            this, &ComboBoxEditorDelegate::onItemActivated);
    box->view()->installEventFilter(const_cast<ComboBoxEditorDelegate*>(this));
    m_box = box;
    return box;
}

void ComboBoxEditorDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QComboBox *box {qobject_cast<QComboBox *>(editor)};
    if (box) {
        QVariant data {getModelBoxData(index)};
        int idx {getBoxIndexByData(box, data)};

        if ((idx < 0) && !m_editable) {
            idx = 0;
        } else {
            m_idx = idx;
        }

        box->setCurrentIndex(idx);
        if (m_editable && (idx < 0)) {
            box->setEditText(data.toString());
        }
    }
}

void ComboBoxEditorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                          const QModelIndex &index) const {
    QComboBox *box {qobject_cast<QComboBox *>(editor)};
    if (box) {
        if (((box->currentIndex()>=0) && (box->currentIndex()!=m_idx)) || (m_editable && (m_idx != -1))) {
            m_idx = box->currentIndex();
            model->setData(index, getBoxData(box, m_idx));
        }
    }
}

void ComboBoxEditorDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                                  const QModelIndex &index) const {
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}


bool ComboBoxEditorDelegate::eventFilter(QObject *object, QEvent *event) {
    bool ret {false};
    QComboBox *box {qobject_cast<QComboBox *>(object)};
    if (box) {
        ret = QStyledItemDelegate::eventFilter(object, event);
        if (event->type() == QEvent::Type::Show) {
             box->showPopup();
        }
    } else {
        if (event->type() == QEvent::Type::KeyPress) {
            QKeyEvent *keyEvet {static_cast<QKeyEvent*>(event)};
            if (keyEvet->key() == Qt::Key::Key_Escape) {
                Q_EMIT closeEditor(m_box);
            }
        }
    }
    return ret;
}

void ComboBoxEditorDelegate::fillBoxItems(QComboBox *box, const QModelIndex &index) const {
    box->addItems(index.data(ItemsTextListRole).toStringList());
}

QVariant ComboBoxEditorDelegate::getBoxData(QComboBox *box, int idx) const {
    Q_UNUSED(idx)
    return box->currentText();
}

QVariant ComboBoxEditorDelegate::getModelBoxData(const QModelIndex &index) const {
    return index.data(Qt::EditRole);
}

int ComboBoxEditorDelegate::getBoxIndexByData(QComboBox *box, const QVariant &data) const {
    int ret {-1};
    const QString str {data.toString()};
    for (int i {0}; (i < box->count()) && (ret < 0); ++i) {
        if (box->itemText(i) == str) {
           ret = i;
        }
    }
    return ret;
}

void ComboBoxEditorDelegate::onItemActivated(int idx) {
    QComboBox *box {qobject_cast<QComboBox *>(sender())};
    if (box && (idx >= 0)) {
        Q_EMIT commitData(box);
        Q_EMIT closeEditor(box);
    }
}


