#ifndef DIALOGDELEGATE_H
#define DIALOGDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>

class QDialog;

/* Базовый класс для создания делегата, который открывает отдельный диалог для
 * редактирования параметра */
class DialogDelegate : public QStyledItemDelegate {
    Q_OBJECT
    using BaseType = QStyledItemDelegate;
public:

    explicit DialogDelegate(QObject *parent = nullptr);
    ~DialogDelegate() override;

    bool editorEvent( QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option,
                      const QModelIndex &index ) override;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;
private Q_SLOTS:
    void onDialogFinished(int result);
protected:
    virtual QDialog *createDialog(QWidget *parent, QAbstractItemModel *model,
                                  const QModelIndex &index) = 0;
    virtual void saveDialogData(QDialog *dialog, QAbstractItemModel *model,
                                const QModelIndex &index) = 0;
private:
    QWidget *m_topWgt;
    QDialog *m_dlg {nullptr};
    QAbstractItemModel *m_model {nullptr};
    QModelIndex m_index;
};

#endif // DIALOGDELEGATE_H
