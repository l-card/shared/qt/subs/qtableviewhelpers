#ifndef COMBOBOXVALSEDITORDELEGATE_H
#define COMBOBOXVALSEDITORDELEGATE_H

#include "ComboBoxEditorDelegate.h"
#include <QList>

class ComboBoxValsEditorDelegate : public ComboBoxEditorDelegate {
    Q_OBJECT
public:
    explicit ComboBoxValsEditorDelegate(QObject *parent = nullptr);

    static const int ItemsValuesListRole {static_cast<int>(Qt::ItemDataRole::UserRole + 1)};

    struct Item {
        Item() {}
        Item(const QString &n, const QVariant &v) : name{n}, value{v} {}

        QString name;
        QVariant value;
    };

    using ComboBoxEditorDelegate::ComboBoxEditorDelegate;

protected:
    void fillBoxItems(QComboBox *box, const QModelIndex &index) const override;
    int getBoxIndexByData(QComboBox *box, const QVariant &data) const override;
    QVariant getBoxData(QComboBox *box, int idx) const override;
};

Q_DECLARE_METATYPE(ComboBoxValsEditorDelegate::Item);
Q_DECLARE_METATYPE(QList<ComboBoxValsEditorDelegate::Item>);

#endif // COMBOBOXVALSEDITORDELEGATE_H
