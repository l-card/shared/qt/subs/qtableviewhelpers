#ifndef MULTIICONDELEGATE_H
#define MULTIICONDELEGATE_H

#include <QItemDelegate>

/* делегат служит для отображения нескольких иконок перед текстом, а не одной.
 * По DecorationRole принимает QList<QIcon>
 * Также выравнивает ширину checkBox и иконок для отображения выравненного
 * теста при использовании checkBox-а как альтернативу иконке */
class MultiIconDelegate : public QItemDelegate {
    Q_OBJECT
public:
    explicit MultiIconDelegate(QObject *parent = nullptr);

    void paint(QPainter * painter,
               const QStyleOptionViewItem & option,
               const QModelIndex & index ) const override;
};

Q_DECLARE_METATYPE(QList<QIcon>);

#endif // MULTIICONDELEGATE_H
