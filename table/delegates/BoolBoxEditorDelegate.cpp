#include "BoolBoxEditorDelegate.h"
#include <QComboBox>

BoolBoxEditorDelegate::BoolBoxEditorDelegate(QObject *parent) : ComboBoxEditorDelegate{parent} {
}

void BoolBoxEditorDelegate::fillBoxItems(QComboBox *box, const QModelIndex &index) const {
    Q_UNUSED(index);
    box->addItem(valueText(true), true);
    box->addItem(valueText(false), false);
}

QVariant BoolBoxEditorDelegate::getBoxData(QComboBox *box, int idx) const {
    Q_UNUSED(idx);
    return box->currentData().toBool();
}

int BoolBoxEditorDelegate::getBoxIndexByData(QComboBox *box, const QVariant &data) const {
    bool val {data.toBool()};
    for (int i {0}; i < box->count(); ++i) {
        if (box->itemData(i).toBool() == val) {
           return i;
        }
    }
    return - 1;
}
