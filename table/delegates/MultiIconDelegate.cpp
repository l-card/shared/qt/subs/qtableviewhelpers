#include "MultiIconDelegate.h"
#include <QApplication>

MultiIconDelegate::MultiIconDelegate(QObject *parent) : QItemDelegate{parent} {

}

void MultiIconDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    const QList<QIcon> &icons {index.data(Qt::ItemDataRole::DecorationRole).value< QList<QIcon> >()};
    const QString text {index.data(Qt::ItemDataRole::DisplayRole).toString()};

    QStyleOptionViewItem opt{option};
    // set font
    QVariant value {index.data(Qt::ItemDataRole::FontRole)};
    if (value.isValid()){
        opt.font = qvariant_cast<QFont>(value).resolve(opt.font);
        opt.fontMetrics = QFontMetrics{opt.font};
    }
    // set text alignment
    value = index.data(Qt::ItemDataRole::TextAlignmentRole);
    if (value.isValid())
        opt.displayAlignment = static_cast<Qt::Alignment>(value.toInt());
    // set foreground brush
    value = index.data(Qt::ItemDataRole::ForegroundRole);
    if (value.canConvert<QBrush>()) {
        opt.palette.setBrush(QPalette::ColorRole::Text, qvariant_cast<QBrush>(value));
    }

    const int margin {1};
    const QSize iconSize {opt.decorationSize};
    int dx {margin};

    QStyleOptionButton checkOpt;
    checkOpt.QStyleOption::operator=(option);
    const QRect checkRect {QApplication::style()->subElementRect(
                    QStyle::SubElement::SE_ItemViewItemCheckIndicator, &checkOpt)};
    /* дополнительные отступы для выравнивания check-box и icon по ширине */
    const int iconSideMargin {qMax((checkRect.width() - iconSize.width())/2, 0)};
    const int chekSideMargin {qMax((iconSize.width() - checkRect.width())/2, 0)};

    drawBackground(painter, opt, index);

    QVariant checkVar {index.data(Qt::ItemDataRole::CheckStateRole)};
    if (checkVar.isValid()) {
        dx += chekSideMargin + margin;

        Qt::CheckState checkState {static_cast<Qt::CheckState>(checkVar.toInt())};
        drawCheck(painter, opt,  QRect{opt.rect.x() + dx,
                                       opt.rect.y() + opt.rect.height()/2 - checkRect.height()/2,
                                       checkRect.width(), checkRect.height()}, checkState);
        dx += checkRect.width() + chekSideMargin + margin;
    }


    for (const QIcon &icon : icons) {
        dx += margin + iconSideMargin;
        drawDecoration(painter, opt, QRect{opt.rect.x() + dx,
                                           opt.rect.y() + opt.rect.height()/2 - iconSize.height()/2,
                                           iconSize.width(), iconSize.height()},
                       icon.pixmap(iconSize));
        dx+= iconSize.width() + margin + iconSideMargin;
    }

    drawDisplay(painter, opt, QRect{opt.rect.x() + dx, opt.rect.y(),
                                    opt.rect.width() - dx, opt.rect.height()},
                text);
    drawFocus(painter, opt, opt.rect);
}
