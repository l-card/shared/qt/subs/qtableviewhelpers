#include "TextValidatorDelegate.h"
#include <QValidator>
#include <QLineEdit>

TextValidatorDelegate::TextValidatorDelegate(QObject *parent, QValidator *validator)
    : QStyledItemDelegate{parent}, m_validator{validator} {

}

QWidget *TextValidatorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                             const QModelIndex &index) const {
    Q_UNUSED(option) Q_UNUSED(index)

    QLineEdit *editor {new QLineEdit{parent}};
    editor->setValidator(m_validator);
    connect(editor, &QLineEdit::textChanged, this, &TextValidatorDelegate::onTextChange);
    return editor;
}

void TextValidatorDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    const QString value {index.model()->data(index, Qt::ItemDataRole::EditRole).toString()};
    QLineEdit *line {static_cast<QLineEdit*>(editor)};
    line->setText(value);
}

void TextValidatorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    QLineEdit *line {static_cast<QLineEdit*>(editor)};
    const QString value{line->text()};
    model->setData(index, value);
}

void TextValidatorDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}

void TextValidatorDelegate::onTextChange(QString text) {
    QLineEdit *editor {qobject_cast<QLineEdit *>(sender())};
    if (editor) {
        int pos = 0;
        if (editor->validator()->validate(text, pos) == QValidator::State::Acceptable) {
            editor->setStyleSheet(QStringLiteral("QLineEdit { color: darkGreen }"));
        } else {
            editor->setStyleSheet(QStringLiteral("QLineEdit { color: darkRed }"));
        }
    }
}
