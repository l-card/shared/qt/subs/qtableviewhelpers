#ifndef COLORDELEGATE_H
#define COLORDELEGATE_H

#include "DialogDelegate.h"

class ColorDelegate : public DialogDelegate {
    Q_OBJECT
public:
    explicit ColorDelegate(QObject *parent = nullptr);

    QDialog *createDialog(QWidget *parent, QAbstractItemModel *model, const QModelIndex &index) override;
    void saveDialogData(QDialog *dialog, QAbstractItemModel *model, const QModelIndex &index) override;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // COLORDELEGATE_H
