#ifndef DOUBLEPRECDELEGATE_H
#define DOUBLEPRECDELEGATE_H

#include <QStyledItemDelegate>

/* Делегат для редактирования значений типа double в DoubleSpinBox.
   В отличие от стандартного делегата позволяет настроить параметры
   элемента (кол-во числе после запятой, шаг приращения, макс. и мин. значения)
   по значениям модели на основе пользовательских ролей */
class DoubleSpinBoxDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    static const int DecimalsRole   { Qt::ItemDataRole::UserRole + 1 };
    static const int StepRole       { Qt::ItemDataRole::UserRole + 2 };
    static const int MaxRole        { Qt::ItemDataRole::UserRole + 3 };
    static const int MinRole        { Qt::ItemDataRole::UserRole + 4 };
    static const int PrefixTextRole { Qt::ItemDataRole::UserRole + 5 };
    static const int SuffixTextRole { Qt::ItemDataRole::UserRole + 6 };

    explicit DoubleSpinBoxDelegate(QObject *parent = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // DOUBLEPRECDELEGATE_H
