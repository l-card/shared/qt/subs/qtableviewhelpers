#include "LedTextDelegate.h"
#include <QPainter>
#include <QApplication>

LedTextDelegate::LedTextDelegate(QObject *parent, DelegateIndexSelector *paintSelector) : LedDelegate{parent, paintSelector} {

}

void LedTextDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    if (!paintEnabled(index)) {
        LedDelegate::paint(painter, option, index);
    } else {
        const int ledSize {qMin(option.rect.width() - 20, option.rect.height())};
        const QRect ledRect{option.rect.left(), option.rect.top(), ledSize, ledSize};

        paintLed(painter, option, ledRect, index);

        const QRect textRect {QRect{option.rect.left() + ledSize, option.rect.top(),
                        option.rect.width() - ledSize, option.rect.height()}};
        const QString text {index.data(Qt::ItemDataRole::DisplayRole).toString()};

        const QVariant bgBrush {index.data(Qt::ItemDataRole::BackgroundRole)};
        if (bgBrush.isValid()) {
            painter->fillRect(textRect, bgBrush.value<QBrush>());
        } else if (option.state & QStyle::StateFlag::State_Selected) {
            painter->fillRect(textRect, option.palette.highlight());
        }
        QApplication::style()->drawItemText(painter, textRect,
                                            Qt::AlignmentFlag::AlignVCenter | Qt::AlignmentFlag::AlignLeft,
                                            option.palette, true, text,
                                            ((option.state & QStyle::StateFlag::State_Selected) ?
                                                 QPalette::ColorRole::HighlightedText :
                                                 QPalette::ColorRole::Text));
    }
}
