#ifndef TEXTVALIDATORDELEGATE_H
#define TEXTVALIDATORDELEGATE_H

#include <QStyledItemDelegate>


class TextValidatorDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    explicit TextValidatorDelegate(QObject *parent, QValidator *validator);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
private Q_SLOTS:
    void onTextChange(QString text);
private:
    QValidator *m_validator;

};

#endif // TEXTVALIDATORDELEGATE_H
