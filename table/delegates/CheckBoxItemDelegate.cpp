#include "CheckBoxItemDelegate.h"
#include "DelegateIndexSelector.h"

#include <QApplication>
#include <QStyleOptionButton>
#include <QPainter>
#include <QEvent>
#include <QMouseEvent>

CheckBoxItemDelegate::CheckBoxItemDelegate(QObject *parent, Qt::AlignmentFlag alignment, DelegateIndexSelector *paintSelector) :
    BaseType{parent},
    m_paintSelector{paintSelector},
    m_align{alignment} {

}

CheckBoxItemDelegate::~CheckBoxItemDelegate() {

}

void CheckBoxItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    if (!m_paintSelector || m_paintSelector->delegatePaintEnabled(index)) {
        // Setting parameters
        QStyleOptionButton btnOpt;

        btnOpt.state = QStyle::StateFlag::State_None;

        if (index.flags() & Qt::ItemFlag::ItemIsEditable) {
            btnOpt.state |= QStyle::StateFlag::State_Enabled;
        } else {
            btnOpt.state |= QStyle::StateFlag::State_ReadOnly;
        }

        if (option.state & QStyle::StateFlag::State_MouseOver)
            btnOpt.state |= QStyle::StateFlag::State_MouseOver; // Mouse over sell
        if (option.state & QStyle::StateFlag::State_Selected)
            btnOpt.state |= QStyle::StateFlag::State_Selected | QStyle::StateFlag::State_MouseOver;

        const Qt::CheckState state {static_cast<Qt::CheckState>(index.data(Qt::ItemDataRole::CheckStateRole).toInt())};
        switch (state) {
        case Qt::CheckState::Unchecked:
            btnOpt.state |= QStyle::StateFlag::State_Off;
            break;
        case Qt::CheckState::PartiallyChecked:
            btnOpt.state |= QStyle::StateFlag::State_NoChange;
            break;
        case Qt::CheckState::Checked:
            btnOpt.state |= QStyle::StateFlag::State_On;
            break;
        }
        // Check box rect
        fillCheckRect(option, btnOpt);

        const QVariant bgBrush {index.data(Qt::ItemDataRole::BackgroundRole)};
        if (bgBrush.isValid()) {
            painter->fillRect(option.rect, bgBrush.value<QBrush>());
        } else if (option.state & QStyle::State_Selected) {
            painter->fillRect(option.rect, option.palette.highlight());
        }

        // Mandatory: drawing check box
        QApplication::style()->drawControl( QStyle::ControlElement::CE_CheckBox, &btnOpt, painter );
    } else {
        BaseType::paint(painter, option, index);
    }
}

bool CheckBoxItemDelegate::editorEvent( QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index ) {
    if (model->flags(index) & Qt::ItemFlag::ItemIsEditable) {
        if (!m_paintSelector || m_paintSelector->delegatePaintEnabled(index)) {
            switch ( event->type() ) {
                case QEvent::Type::MouseButtonPress:
                    m_lastClickedIndex = index;
                    break;
                case QEvent::Type::MouseButtonRelease: {
                        if (index != m_lastClickedIndex)
                            break;
                        QMouseEvent *e {static_cast<QMouseEvent *>(event)};
                        if (e->button() != Qt::MouseButton::LeftButton)
                            break;

                        m_lastClickedIndex = QModelIndex{};

                        QStyleOptionButton btnOpt;
                        fillCheckRect(option, btnOpt);

                        if (btnOpt.rect.contains(e->pos())) {
                            Qt::CheckState state {static_cast<Qt::CheckState>(
                                            index.data( Qt::ItemDataRole::CheckStateRole ).toInt())};
                            state = (state == Qt::CheckState::Checked ? Qt::CheckState::Unchecked : Qt::CheckState::Checked);
                            model->setData(index, state, Qt::ItemDataRole::CheckStateRole);
                        }
                    }
                    break;
            case QEvent::Type::MouseButtonDblClick:
                    return true;
                default:
                    break;
            }
        }
    }
    return BaseType::editorEvent( event, model, option, index );
}

void CheckBoxItemDelegate::fillCheckRect(const QStyleOptionViewItem &option, QStyleOptionButton &btnOpt) const {
    btnOpt.rect = QApplication::style()->subElementRect(
                QStyle::SubElement::SE_CheckBoxIndicator, &btnOpt, nullptr);
    const int y {option.rect.center().y() - btnOpt.rect.height() / 2};
    int x;
    if (m_align & Qt::AlignmentFlag::AlignLeft) {
        x = option.rect.left() + btnOpt.rect.width() / 2;
    } else if (m_align & Qt::AlignmentFlag::AlignRight) {
        x = option.rect.right() - 3 * btnOpt.rect.width() / 2;
    } else {
        x = option.rect.center().x() - btnOpt.rect.width() / 2;
    }
    btnOpt.rect.moveTo(x, y);
}
