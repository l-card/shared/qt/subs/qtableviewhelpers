#ifndef TABLEPARAMSMODELHELPER_H
#define TABLEPARAMSMODELHELPER_H

#include "params/TableParameter.h"
#include "TableParamsModelIface.h"
#include <QTableView>

/* макрос должен быть вставлен внутри определения модели, унаследованной от
 * QAbstractTableModel и TableParamsModelHelper, и определяет стандартные методы
 * от QAbstractTableModel */
#define TABLE_PARAM_MODEL_METHODS \
    int rowCount(const QModelIndex &parent = QModelIndex()) const override { Q_UNUSED(parent); return paramItemsCount();} \
    int columnCount(const QModelIndex &parent = QModelIndex()) const override {Q_UNUSED(parent); return paramCount();} \
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override {return paramData(index, role); } \
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole ) override { \
        return paramSetData(this, index, value, role); \
    } \
    QVariant headerData(int section, Qt::Orientation orientation, \
                        int role = Qt::DisplayRole ) const override { \
        return paramHeaderData(section, orientation, role); \
    } \
    Qt::ItemFlags flags(const QModelIndex &index) const override {return paramFlags(index);} \
    void updateItem(int item_idx) override { \
        paramItemsUpdate(item_idx, item_idx); \
    } \
    void updateItemParams(int item_idx, int param_idx_first, int param_idx_last) override { \
        Q_EMIT dataChanged(createIndex(item_idx, param_idx_first), createIndex(item_idx, param_idx_last)); \
    } \
    void paramItemsBeginInsert(int first, int last)  override { \
        beginInsertRows(QModelIndex(), first, last); \
    } \
    void paramItemsEndInsert() override { \
        endInsertRows();\
    } \
    void paramItemsBeginRemove(int first, int last) override {\
        beginRemoveRows(QModelIndex(), first, last); \
    } \
    void paramItemsEndRemove() override { \
        endRemoveRows(); \
    } \
    void paramItemsUpdate(int first, int last) override { \
        Q_EMIT dataChanged(createIndex(first, 0), createIndex(last, paramCount()-1)); \
    } \
    int paramDefaultColumnWidth(int param_idx) override { \
        return paramAt(param_idx)->columnWidth(); \
    } \
    QAbstractItemDelegate *paramCreateDelegate(int param_idx, QTableView *view) override {\
        return paramAt(param_idx)->createDelegate(view); \
    } \
    QAbstractItemModel *paramItemModel() override {\
        return static_cast<QAbstractItemModel *>(this); \
    }



template <typename ParamItem> class TableParamsModelHelper {
public:
    virtual ~TableParamsModelHelper() {
        for (int i = m_items.size()-1; i >=0; i--)
            paramItemRem(i);
        qDeleteAll(m_params);
    }

    void paramInitView(QTableView *view) const;
    void paramInitView(QTableView *view, int first_col, int last_col) const;

    int paramCount() const {return m_params.size();}
    int paramItemsCount() const {return m_items.size();}

    bool readOnly() const {return m_read_only;}
    void setReadOnly(bool ro) {m_read_only = ro;}

    void paramItemsDelete() {
        QList<ParamItem *> items = m_items;
        paramItemsClear();
        qDeleteAll(items);
    }

    void paramItemsClear() {
        while (!m_items.isEmpty())
            paramItemRem(0);
    }

    ParamItem *paramItem(int idx) const {
        return m_items.at(idx);
    }

    void paramItemRem(int idx) {
        paramOnItemRemove(idx, paramItem(idx));
        m_items.removeAt(idx);
    }

    void paramItemAdd(ParamItem *item) {
        m_items.append(item);
        paramOnItemInsert(m_items.size()-1, item);
    }

    void paramItemInsert(int idx, ParamItem *item) {
        m_items.insert(idx, item);  paramOnItemInsert(idx, item);
    }

    void paramItemReplace(int idx, ParamItem *item) {
        paramOnItemRemove(idx, paramItem(idx));
        m_items.replace(idx, item);
        paramOnItemInsert(idx, item);
    }

    QVariant paramData(QModelIndex index, int role) const;
    bool paramSetData(TableParamsModelIface *model, const QModelIndex &index,
                      const QVariant &value, int role);


    QVariant paramHeaderData(int section, Qt::Orientation orientation,
                             int role = Qt::DisplayRole ) const {
        QVariant ret;        
        if (orientation == Qt::Orientation::Horizontal) {
            if ((section >= 0) && (section < m_params.size())) {
                if (role == Qt::ItemDataRole::DisplayRole)
                    return m_params.at(section)->name();
                if (role == Qt::ItemDataRole::ToolTipRole)
                    return m_params.at(section)->paramToolTip();
            }
        }
        return ret;
    }

    Qt::ItemFlags paramFlags(const QModelIndex &index) const {
        Qt::ItemFlags flags {Qt::ItemFlag::NoItemFlags};
        if (index.isValid()) {
            flags = m_params.at(index.column())->flags(m_items.at(index.row()));
            if (m_read_only) {
                flags &= ~Qt::ItemFlag::ItemIsEditable;
            }
        }
        return flags;
    }

    typedef TableParameter<ParamItem *> Param;

    const Param *paramAt(int idx) const {return m_params.at(idx);}
    Param *paramAt(int idx) {return m_params.at(idx);}

    QList<int> paramIndexList(bool (*check_cb)(const Param *)) const;

    int paramItemNum(const ParamItem *item) const {return paramItemList().indexOf(item);}
    int paramNum(Param *param) const {return m_params.indexOf(param);}

    const QList<ParamItem *> &paramItemList()  {return m_items;}
    const QList<const ParamItem *> &paramItemList() const {
        return reinterpret_cast<const QList<const ParamItem *> &>(m_items);
    }
protected:
    void paramAdd(Param *param);
    void paramInsert(int idx, Param *param);
    void paramRem(int idx) {delete m_params.takeAt(idx);}
    void paramClear() {
        qDeleteAll(m_params);
        m_params.clear();
    }

    /* данные функции вызываются всякий раз после добавления или перед удалением
       элемента соответственно и служат для того, чтобы их можно было переопределить
       в унаследованном классе для выполнения каких-либо доп. действий, например
       подключения сигналов о изменении параметров и т.п. */
    virtual void paramOnItemInsert(int idx, ParamItem *item) {
        Q_UNUSED(idx)
        Q_UNUSED(item)
    }
    virtual void paramOnItemRemove(int idx, ParamItem *item) {
        Q_UNUSED(idx)
        Q_UNUSED(item)
    }
private:
    bool m_read_only {false};
    QList<Param *> m_params;
    QList<ParamItem *> m_items;
};



template <typename ParamItem>  void TableParamsModelHelper<ParamItem>::paramInitView(QTableView *view) const {
    paramInitView(view, 0, m_params.size()-1);
}

template <typename ParamItem>  void TableParamsModelHelper<ParamItem>::paramInitView(QTableView *view, int first_col, int last_col) const {
    for (int col {first_col}; col <= last_col; col++) {
        QAbstractItemDelegate *deletgate {m_params.at(col)->createDelegate(view)};
        if (deletgate) {
            view->setItemDelegateForColumn(col, deletgate);
        }
        if (m_params.at(col)->columnWidth() > 0)
            view->setColumnWidth(col, m_params.at(col)->columnWidth());
    }
}

template <typename ParamItem> QVariant TableParamsModelHelper<ParamItem>::paramData(QModelIndex index, int role) const {
    QVariant ret;
    if (index.isValid()) {
        ret = m_params.at(index.column())->data(m_items.at(index.row()), role);
    }
    return ret;
}

template <typename ParamItem> bool TableParamsModelHelper<ParamItem>::paramSetData(
        TableParamsModelIface *model, const QModelIndex &index, const QVariant &value, int role) {
    bool ok = false;
    if (index.isValid()) {
        ok = m_params.at(index.column())->setData(m_items.at(index.row()), value, role);
        if (ok) {
            if (m_params.at(index.column())->itemUpdateRequired()) {
                model->updateItem(index.row());
            } else if (m_params.at(index.column())->itemParamUpdateReqired()) {
                model->updateItemParam(index.row(), index.column());
            }
        }
    }
    return ok;
}

template <typename ParamItem> QList<int> TableParamsModelHelper<ParamItem>::paramIndexList(bool (*check_cb)(const Param *)) const {
    QList<int> ret;
    for (int param_idx = 0; param_idx < paramCount(); param_idx++) {
        if (check_cb(paramAt(param_idx))) {
            ret.append(param_idx);
        }
    }
    return ret;
}

template <typename ParamItem> void TableParamsModelHelper<ParamItem>::paramAdd(Param *param) {
    m_params.append(param);
}

template <typename ParamItem> void TableParamsModelHelper<ParamItem>::paramInsert(int idx, Param *param) {
    m_params.insert(idx, param);
}


#endif // TABLEPARAMSMODELHELPER_H
