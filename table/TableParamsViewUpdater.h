#ifndef TABLEPARAMSVIEWUPDATER_H
#define TABLEPARAMSVIEWUPDATER_H

#include <QObject>
#include "TableViewSettings.h"
class QTableView;
class TableParamsModelIface;
class QSettings;
class QAbstractItemModel;

/* Класс для управления TableView в соответствии с параметрами модули, заданной
 * через TableParamsModelHelper/TableParameter
 * Доступ к параметрам осуществляется через интерфейс TableParamsModelIface.
 * По умолчанию таблице назначается модель данных из TableParamsModelIface::paramItemModel(),
 * однако ее можно задать и явно, для назначения таблице прокси-модели, которая
 * может использоваться например для фильтрации/сортировки строк основной модели
 * параметров (при этом структура столбцов должна соответствовать основной модели
 * параметров).
*/
class TableParamsViewUpdater : public QObject {
    Q_OBJECT
public:
    TableParamsViewUpdater(QTableView *view, TableParamsModelIface *modelIface,
                           QAbstractItemModel *model = nullptr);

    void loadState(const QSettings &set, const QString &key);
    void reloadState();
    void reloadState(const QSettings &set, const QString &key);
    void saveState() const;
    void saveState(QSettings &set, const QString &key) const;
    void addCopyAction();
private Q_SLOTS:
    void onParamsInserted(const QModelIndex &parent, int first, int last);
    void preModelReset();
    void onModelReset();
Q_SIGNALS:
    void languageChanged();
protected:
    bool eventFilter(QObject *obj, QEvent *event) override;
private:
    void initRows(int from = 0, int to = -1);

    QTableView *m_view;
    QString m_setgroup, m_name;
    TableParamsModelIface *m_modelIface;
    QAbstractItemModel *m_model;

};

#endif // TABLEPARAMSVIEWUPDATER_H
