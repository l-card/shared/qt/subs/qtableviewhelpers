#include "TableParamsView.h"
#include "TableParamsViewUpdater.h"
#include "misc/TableAutoScroller.h"
#include "misc/TableRowChangeSelector.h"

TableParamsView::TableParamsView(QWidget *parent) : QTableView{parent} {

}

void TableParamsView::init(TableParamsModelIface *modelIface, QAbstractItemModel *model) {
    m_updater = new TableParamsViewUpdater{this, modelIface, model};
}

void TableParamsView::loadState(const QSettings &set, const QString &key) {
    if (m_updater) {
        m_updater->loadState(set, key);
    }
}

void TableParamsView::saveState(QSettings &set, const QString &key) const {
    if (m_updater) {
        m_updater->saveState(set, key);
    }
}

void TableParamsView::saveState() const {
    if (m_updater) {
        m_updater->saveState();
    }
}

void TableParamsView::reloadState(const QSettings &set, const QString &key) {
    if (m_updater) {
        m_updater->reloadState(set, key);
    }
}

void TableParamsView::reloadState() {
    if (m_updater) {
        m_updater->reloadState();
    }
}

void TableParamsView::addCopyAction() {
    if (m_updater) {
        m_updater->addCopyAction();
    }
}

void TableParamsView::setReadOnly(bool en) {
    if (en) {
        setEditTriggers(QAbstractItemView::EditTrigger::NoEditTriggers);
    } else {
        setEditTriggers(QAbstractItemView::EditTrigger::DoubleClicked |
                        QAbstractItemView::EditTrigger::EditKeyPressed |
                        QAbstractItemView::EditTrigger::AnyKeyPressed);
    }
}

void TableParamsView::enableHeightAutoUpdate(TableHeightUpdater::Flags flags) {
    if (!m_high_updater) {
        m_high_updater = new TableHeightUpdater{this, flags};
    }
}

void TableParamsView::enableAutoScroll() {
    enableAutoScroll(TableAutoScroller::scrollMode());
}

void TableParamsView::enableAutoScroll(const TableAutoScrollMode &mode) {
    new TableAutoScroller{this, mode};
}

void TableParamsView::enableChangedRowAutoSelect() {
    new TableRowChangeSelector{this};
}

void TableParamsView::highUpdate() {
    if (m_high_updater) {
        m_high_updater->updateHeight();
    }
}

void TableParamsView::updateIndex(const QModelIndex &idx) {
    QTableView::update(idx);
}

int TableParamsView::selectedRow() const {
    int row {-1};
    const QModelIndexList &idxList {selectionModel()->selectedIndexes()};
    if (!idxList.isEmpty()) {
        row = idxList.first().row();
    }
    return  row;
}
