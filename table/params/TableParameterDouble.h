#ifndef TABLEPARAMETERDOUBLE_H
#define TABLEPARAMETERDOUBLE_H

#include "TableParameter.h"
#include "../delegates/DoubleSpinBoxDelegate.h"
#include <QLocale>
#include <QStringBuilder>

template <typename ParamItem> class TableParameterDouble : public TableParameter<ParamItem> {
protected:
    TableParameterDouble() {
        m_locale.setNumberOptions(m_locale.numberOptions() | QLocale::NumberOption::OmitGroupSeparator);
    }

    virtual double paramValue(ParamItem item) const = 0;
    virtual bool paramSetValue(ParamItem item, double val) const = 0;


    virtual int paramDecimals(ParamItem item) const = 0;
    virtual double paramStepValue(ParamItem item) const = 0;
    virtual double paramMaxValue(ParamItem item) const = 0;
    virtual double paramMinValue(ParamItem item) const = 0;
    virtual int paramTextDecimals(ParamItem item) const {return paramDecimals(item);}
    virtual QString paramPrefixText(ParamItem item) const {
        Q_UNUSED(item)
        return QString{};
    }
    virtual QString paramSuffixText(ParamItem item) const {
        Q_UNUSED(item)
        return QString{};
    }
    /* признак, разрешено ли показывать значение.
     * Если true, выводится paramValue() с префиксом и суффиксом,
     * иначе - paramSpecialValueText() */
    virtual bool paramEnableShowValue(ParamItem item) const {
        Q_UNUSED(item)
        return true;
    }
    virtual QString paramSpecialValueText(ParamItem item) const {
        Q_UNUSED(item)
        return QString{};
    }



    QVariant protData(ParamItem item, int role) const override {
        QVariant ret;
        if (role == Qt::ItemDataRole::DisplayRole) {
            if (paramEnableShowValue(item)) {
                double val = paramValue(item);
                const QString prefix {paramPrefixText(item)};
                const QString suffix {paramSuffixText(item)};
                const int decimals {paramTextDecimals(item)};
                const QString retStr {prefix % m_locale.toString(val, 'f', decimals) % suffix};
                ret = retStr;
            } else {
                ret = paramSpecialValueText(item);
            }
        } else if (role == Qt::ItemDataRole::EditRole) {
            ret = paramValue(item);
        } else if (role == DoubleSpinBoxDelegate::DecimalsRole) {
            ret = paramDecimals(item);
        } else if (role == DoubleSpinBoxDelegate::StepRole) {
            ret = paramStepValue(item);
        } else if (role == DoubleSpinBoxDelegate::MaxRole) {
            ret = paramMaxValue(item);
        } else if (role == DoubleSpinBoxDelegate::MinRole) {
            ret = paramMinValue(item);
        } else if (role == DoubleSpinBoxDelegate::PrefixTextRole) {
            ret = paramPrefixText(item);
        } else if (role == DoubleSpinBoxDelegate::SuffixTextRole) {
            ret = paramSuffixText(item);
        }
        return ret;
    }

    bool protSetData(ParamItem item, const QVariant &value, int role) const override {
        bool ok {false};
        if (role == Qt::ItemDataRole::EditRole) {
            ok = paramSetValue(item, value.toDouble());
        }
        return ok;
    }

    QAbstractItemDelegate *createDelegate(QObject *parent) const override {
        return new DoubleSpinBoxDelegate{parent};
    }
protected:
    QLocale m_locale;
};

#endif // TABLEPARAMETERDOUBLE_H
