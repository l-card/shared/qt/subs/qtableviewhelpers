#ifndef TABLEPARAMETERBOOL_H
#define TABLEPARAMETERBOOL_H

#include "TableParameter.h"
#include "../delegates/BoolBoxEditorDelegate.h"
#include "../delegates/CheckBoxItemDelegate.h"

template <typename ParamItem> class TableParameterBool : public TableParameter<ParamItem> {
public:
    int columnWidth() const override {return 40;}
protected:
    virtual bool paramValue(ParamItem item) const = 0;
    virtual bool paramSetValue(ParamItem item, bool en) const = 0;

    QVariant protData(ParamItem item, int role) const override {
        QVariant ret;
        if ((role == Qt::DisplayRole) ||
                (role == Qt::EditRole) ||
                (role == Qt::CheckStateRole)) {
            bool on = paramValue(item);
            if (role == Qt::DisplayRole) {
                ret = BoolBoxEditorDelegate::defaultValText(on);
            } else if (role == Qt::EditRole) {
                ret = on;
            } else if (role == Qt::CheckStateRole) {
                ret = on ? Qt::Checked : Qt::Unchecked;
            }
        }
        return ret;
    }

    bool protSetData(ParamItem item, const QVariant &value, int role) const override {
        bool ok = false;
        if (role == Qt::CheckStateRole) {
            ok = paramSetValue(item, value.toInt() == Qt::Checked);
        }
        return ok;
    }

    QAbstractItemDelegate *createDelegate(QObject *parent) const override {
        return new CheckBoxItemDelegate(parent);
    }

    bool itemParamUpdateReqired() const override {return true;}
};

#endif // TABLEPARAMETERBOOL_H
