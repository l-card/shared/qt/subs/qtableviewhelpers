#ifndef TABLEPARAMETERCOMBOBOXVALS_H
#define TABLEPARAMETERCOMBOBOXVALS_H

#include "TableParameter.h"
#include "../delegates/ComboBoxValsEditorDelegate.h"

template <typename ParamItem> class TableParameterComboBoxVals : public TableParameter<ParamItem> {
protected:
    virtual QList<ComboBoxValsEditorDelegate::Item> allValues(ParamItem item) const = 0;
    virtual ComboBoxValsEditorDelegate::Item paramValue(ParamItem item) const = 0;
    virtual bool paramSetValue(ParamItem item, const ComboBoxValsEditorDelegate::Item &value) const = 0;

    QVariant protData(ParamItem item, int role) const override {
        QVariant ret;
        if (role == Qt::ItemDataRole::DisplayRole) {
            ret = paramValue(item).name;
        } else if (role == Qt::ItemDataRole::EditRole) {
            ret = QVariant::fromValue(paramValue(item));
        } else if (role == ComboBoxValsEditorDelegate::ItemsValuesListRole) {
            ret = QVariant::fromValue(allValues(item));
        }
        return ret;
    }

    bool protSetData(ParamItem item, const QVariant &value, int role) const override {
        bool ok = false;
        if (role == Qt::ItemDataRole::EditRole) {
            ok = paramSetValue(item, value.value<ComboBoxValsEditorDelegate::Item>());
        }
        return ok;
    }

    /* запрещаем редактирование, если количество вариантов меньше 2 */
    Qt::ItemFlags flags(ParamItem item) const override {
        Qt::ItemFlags f {TableParameter<ParamItem>::flags(item)};
        if ((f & Qt::ItemFlag::ItemIsEditable) && (allValues(item).size() < 2)) {
            f &= ~Qt::ItemFlag::ItemIsEditable;
        }
        return f;
    }

    QAbstractItemDelegate *createDelegate(QObject *parent) const override {
        return new ComboBoxValsEditorDelegate{parent};
    }
};

#endif // TABLEPARAMETERCOMBOBOXVALS_H
