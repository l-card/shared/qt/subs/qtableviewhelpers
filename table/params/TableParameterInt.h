#ifndef TABLEPARAMETERINT_H
#define TABLEPARAMETERINT_H

#include "TableParameter.h"
#include "../delegates/SpinBoxDelegate.h"
#include <QStringBuilder>

template <typename ParamItem> class TableParameterInt : public TableParameter<ParamItem> {
protected:
    virtual int paramValue(ParamItem item) const = 0;
    virtual bool paramSetValue(ParamItem item, int val) const = 0;

    virtual int paramStepValue(ParamItem item) const = 0;
    virtual int paramMaxValue(ParamItem item) const = 0;
    virtual int paramMinValue(ParamItem item) const = 0;
    virtual QString paramSpecialValueText(ParamItem item) const {
        Q_UNUSED(item)
        return QString{};
    }
    virtual QString paramPrefixText(ParamItem item) const {
        Q_UNUSED(item)
        return QString{};
    }
    virtual QString paramSuffixText(ParamItem item) const {
        Q_UNUSED(item)
        return QString{};
    }

    QVariant protData(ParamItem item, int role) const override {
        QVariant ret;
        if (role == Qt::ItemDataRole::DisplayRole) {
            const int val {paramValue(item)};
            const QString specText {paramSpecialValueText(item)};
            if (!specText.isEmpty() && (val <= paramMinValue(item))) {
                ret = specText;
            } else {
                const QString prefix {paramPrefixText(item)};
                const QString suffix {paramSuffixText(item)};
                const QString retStr {prefix % QString::number(val) % suffix};
                ret = retStr;
            }
        } else if (role == Qt::EditRole) {
            ret = paramValue(item);
        } else if (role == SpinBoxDelegate::StepRole) {
            ret = paramStepValue(item);
        } else if (role == SpinBoxDelegate::MaxRole) {
            ret = paramMaxValue(item);
        } else if (role == SpinBoxDelegate::MinRole) {
            ret = paramMinValue(item);
        } else if (role == SpinBoxDelegate::SpecialValueTextRole) {
            ret = paramSpecialValueText(item);
        } else if (role == SpinBoxDelegate::PrefixTextRole) {
            ret = paramPrefixText(item);
        } else if (role == SpinBoxDelegate::SuffixTextRole) {
            ret = paramSuffixText(item);
        }
        return ret;
    }

    bool protSetData(ParamItem item, const QVariant &value, int role) const override {
        bool ok {false};
        if (role == Qt::ItemDataRole::EditRole) {
            ok = paramSetValue(item, value.toInt());
        }
        return ok;
    }

    QAbstractItemDelegate *createDelegate(QObject *parent) const override {
        return new SpinBoxDelegate{parent};
    }
};



#endif // TABLEPARAMETERINT_H
