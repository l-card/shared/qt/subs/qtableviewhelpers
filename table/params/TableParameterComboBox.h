#ifndef TABLEPARAMETERCOMBOBOX_H
#define TABLEPARAMETERCOMBOBOX_H


#include "TableParameter.h"
#include "../delegates/ComboBoxEditorDelegate.h"
template <typename ParamItem> class TableParameterComboBox : public TableParameter<ParamItem> {
protected:
    virtual QStringList allValueNames(ParamItem item) const = 0;
    virtual QString paramValueName(ParamItem item) const = 0;
    virtual bool paramSetValueName(ParamItem item, const QString &name) const = 0;

    QVariant protData(ParamItem item, int role) const override {
        QVariant ret;
        if ((role == Qt::ItemDataRole::DisplayRole)
                || (role == Qt::ItemDataRole::EditRole)) {
            ret = paramValueName(item);
        } else if (role == ComboBoxEditorDelegate::ItemsTextListRole) {
            ret = allValueNames(item);
        }
        return ret;
    }

    bool protSetData(ParamItem item, const QVariant &value, int role) const override {
        bool ok = false;
        if (role == Qt::ItemDataRole::EditRole) {
            ok = paramSetValueName(item, value.toString());
        }
        return ok;
    }

    /* запрещаем редактирование, если количество вариантов меньше 2 */
    Qt::ItemFlags flags(ParamItem item) const override {
        Qt::ItemFlags f {TableParameter<ParamItem>::flags(item)};
        if ((f & Qt::ItemFlag::ItemIsEditable) && (allValueNames(item).size() < 2)) {
            f &= ~Qt::ItemFlag::ItemIsEditable;
        }
        return f;
    }

    QAbstractItemDelegate *createDelegate(QObject *parent) const override {
        return new ComboBoxEditorDelegate{parent};
    }
};

#endif // TABLEPARAMETERCOMBOBOX_H
