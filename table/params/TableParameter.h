#ifndef TABLEPARAMETER_H
#define TABLEPARAMETER_H

#include <QVariant>
#include "../TableViewSettings.h"

class QAbstractItemDelegate;

/* Базовый класс для представления логики работы с параметрами на основе
   Qt-ной концепции модуль-представление.  Кроме того, класс может предоставлять
   делегат для редактирования параметра и рекомендуемые параметры отображения
   (ширина столбца).

   Класс стандартную обработку флагов и фона отображения на основе
   состояний enabled(), readOnly(), notAssigned(), isCheckable().

   Унаследованный класс должен при необходимости переопределить эти функции, а
   также реализовать protData() для отображения данных, а также для не readOnly()
   параметров функцию protSetData(). Вызов protData() и protSetData() выполняется
   только в случае, если элемент разрешен (не разрешенные отображаются одинаково).

   Реализация работы c CheckState через isCheckable(), checkState() и setCheckState(). */
template <typename ParamItem> class TableParameter {
public:
    virtual ~TableParameter() {}
    /* Имя параметра (которое будет отображаться в заголовке) */
    virtual QString name() const = 0;
    /* получение данных с обслуживанием некоторых стандартных запросов.
       Как правило не переопределяется, а паереопределяется protData(),
       которая вызывается только для разрешенного элемента, если данный
       запрос не обрабатывается стандартным образом */
    virtual QVariant data(ParamItem item, int role) const {
        QVariant ret;
        bool processed = false;

        if (!enabled(item)) {
            processed = true;
            if (role == Qt::ItemDataRole::BackgroundRole) {
                ret = TableViewSettings::instance()->disabledTableElementBrush();
            }
        } else if (readOnly(item) && (role == Qt::ItemDataRole::BackgroundRole) && !customReadonlyColor(item)) {
            processed = true;
            ret = TableViewSettings::instance()->readonlyTableElementBrush();
        } else if (notAssigned(item) && (role == Qt::ItemDataRole::BackgroundRole)) {
            processed = true;
            ret = TableViewSettings::instance()->notAssignedTableElementBrush();
        } else if (role == Qt::ItemDataRole::ToolTipRole) {
            const QString tip {toolTip(item)};
            if (!tip.isEmpty()) {
                ret = tip;
                processed = true;
            }
        } else if (role == Qt::ItemDataRole::CheckStateRole) {
            if (isCheckable(item)) {
                ret = checkState(item);
                processed = true;
            }
        }

        if (!processed) {
            ret = protData(item, role);
        }
        return ret;
    }

    /* установка данных с проверкой readOnly() и enabled(). Как правило
       не переопределяется, а в унаследованном классе определяется
       setProtData() */
    virtual bool setData(ParamItem item, const QVariant &value, int role) const {
        bool ok {enabled(item) && !readOnly(item)};
        if (ok) {
            if ((role == Qt::ItemDataRole::CheckStateRole) && isCheckable(item)) {
                ok = setCheckState(item, static_cast<Qt::CheckState>(value.toInt()));
            } else {
                ok = protSetData(item, value, role);
            }
        }
        return ok;
    }

    /* Флаги элемента. Определяются на основе других методов, а этот как правило
       не переопределяется */
    virtual Qt::ItemFlags flags(ParamItem item) const {
        Qt::ItemFlags ret = Qt::ItemFlag::ItemIsEnabled | Qt::ItemFlag::ItemIsSelectable;
        if (enabled(item) && !readOnly(item)) {
            ret |= Qt::ItemFlag::ItemIsEditable;
        }
        if (enabled(item) && isCheckable(item)) {
            ret |= Qt::ItemFlag::ItemIsUserCheckable;
        }
        return ret;
    }

    virtual QString toolTip(ParamItem item) const {
        Q_UNUSED(item)
        return QString();
    }

    virtual QVariant paramId() const {return 0;}
    virtual QString paramToolTip() const {return QString();}

    /* Функция создания нестандартного делегата для редоктирования параметра */
    virtual QAbstractItemDelegate *createDelegate(QObject *parent) const {
        Q_UNUSED(parent)
        return Q_NULLPTR;
    }
    /* Рекомендуемая ширина столбца для параметра (если 0 - по-умолчанию) */
    virtual int columnWidth() const {return 0;}
    /* признак, что при изменении параметра необходимо обновить отображения
       всех параметров элемента */
    virtual bool itemUpdateRequired() const {return false;}
    /* признак, что при изменении нужно обновить только отображение изменненного
       параметра элемента */
    virtual bool itemParamUpdateReqired() const {return false;}
    /* признак, разрешен ли параметр для заданного элемента. если нет, то
       не ведется работа с данными, а просто ячейка отображается стандартным
       способом. По умолчанию все разрешены  */
    virtual bool enabled(ParamItem item) const {
        Q_UNUSED(item)
        return true;
    }
    /* признак, что данный параметр доступен только для чтения.
       По умолчанию всегда false.
       Функция вызывается только для разрешенных элеметнов (т.е. при
       реализации можно считать, что enabled() выполняется).
       Если true, то данные отображаются, но их нельзя изменить и используется
       стандартный фон, если только customReadonlyColor() не возвращает true */
    virtual bool readOnly(ParamItem item) const {
        Q_UNUSED(item)
        return false;
    }
    /* Признак, что элемент нужно выделить спец. образом, чтобы показать,
       что параметр не назначен */
    virtual bool notAssigned(ParamItem item) const {
        Q_UNUSED(item)
        return false;
    }
    /* Признак, что для readOnly() элемента не нужно обрабатывать фон стандартным
       образом */
    virtual bool customReadonlyColor(ParamItem item) const {
        Q_UNUSED(item)
        return false;
    }

    virtual bool isCheckable(ParamItem item) const {return false;}
    virtual Qt::CheckState checkState(ParamItem item) const {return Qt::CheckState::Unchecked;}
    virtual bool setCheckState(ParamItem item, Qt::CheckState checkState) const {return false;}

    QString colorToolTipText(QString text, QColor color) const {
        return QString("<div style= \"white-space: nowrap;\"><font color=\"%1\">%2</font></div>")
            .arg(color.name(), text);
    }
protected:
    /* Получение данных, которые не обрабатываются стандартным образом.
       Вызывается только для разрешенного элемента.
       Должно всегда определяться в реализации */
    virtual QVariant protData(ParamItem item, int role) const = 0;
    /* Установка данных. Вызывается после проверки резрешения элемента и что
       он не в состоянии readOnly(). Должен быть переопределен для всех параметров,
       кроме тех, что для всех элементов возвращают в readOnly() true */
    virtual bool protSetData(ParamItem item, const QVariant &value, int role) const {
        Q_UNUSED(item)
        Q_UNUSED(value)
        Q_UNUSED(role)
        return false;
    }
};

#endif // TABLEPARAMETER_H

