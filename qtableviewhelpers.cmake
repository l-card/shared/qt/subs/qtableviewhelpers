set(QTABLEVIEWHELPERS_DIR ${CMAKE_CURRENT_LIST_DIR})

set(QTABLEVIEWHELPERS_SOURCES
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsModelHelper.h
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsModelIface.h
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsModelIface.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsUpdatedModel.h
    ${QTABLEVIEWHELPERS_DIR}/table/TableViewSettings.h
    ${QTABLEVIEWHELPERS_DIR}/table/TableViewSettings.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsViewUpdater.h
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsViewUpdater.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsView.h
    ${QTABLEVIEWHELPERS_DIR}/table/TableParamsView.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/misc/TableHeightUpdater.h
    ${QTABLEVIEWHELPERS_DIR}/table/misc/TableHeightUpdater.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/misc/TableAutoScroller.h
    ${QTABLEVIEWHELPERS_DIR}/table/misc/TableAutoScroller.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/misc/TableRowChangeSelector.h
    ${QTABLEVIEWHELPERS_DIR}/table/misc/TableRowChangeSelector.cpp

    ${QTABLEVIEWHELPERS_DIR}/table/params/TableParameter.h
    ${QTABLEVIEWHELPERS_DIR}/table/params/TableParameterBool.h
    ${QTABLEVIEWHELPERS_DIR}/table/params/TableParameterInt.h
    ${QTABLEVIEWHELPERS_DIR}/table/params/TableParameterComboBox.h
    ${QTABLEVIEWHELPERS_DIR}/table/params/TableParameterComboBoxVals.h
    ${QTABLEVIEWHELPERS_DIR}/table/params/TableParameterDouble.h


    ${QTABLEVIEWHELPERS_DIR}/tree/TreeParamsBaseItem.h
    ${QTABLEVIEWHELPERS_DIR}/tree/TreeParamsBaseItem.cpp
    ${QTABLEVIEWHELPERS_DIR}/tree/TreeParamsBaseModel.h
    ${QTABLEVIEWHELPERS_DIR}/tree/TreeParamsBaseModel.cpp
    ${QTABLEVIEWHELPERS_DIR}/tree/TreeParamsSelectedItem.h
    ${QTABLEVIEWHELPERS_DIR}/tree/TreeParamsSelectedItem.cpp

    ${QTABLEVIEWHELPERS_DIR}/table/delegates/BoolBoxEditorDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/BoolBoxEditorDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/CheckBoxItemDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/CheckBoxItemDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/ComboBoxEditorDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/ComboBoxEditorDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/ComboBoxValsEditorDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/ComboBoxValsEditorDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/DoubleSpinBoxDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/DoubleSpinBoxDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/SpinBoxDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/SpinBoxDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/DialogDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/DialogDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/DelegateIndexSelector.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/TextValidatorDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/TextValidatorDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/LedDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/LedDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/LedTextDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/LedTextDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/ColorDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/ColorDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/MultiIconDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/MultiIconDelegate.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/PushButtonDelegate.h
    ${QTABLEVIEWHELPERS_DIR}/table/delegates/PushButtonDelegate.cpp

    ${QTABLEVIEWHELPERS_DIR}/table/actions/TableCopyAction.cpp
    ${QTABLEVIEWHELPERS_DIR}/table/actions/TableCopyAction.h
    )


include(${QTABLEVIEWHELPERS_DIR}/translations/cmake/qtranslation.cmake)

set(QTABLEVIEWHELPERS_TS_BASENAME qtableviewhelpers)
qtranslation_generate(QTABLEVIEWHELPERS_TRANSLATION_FILES ${QTABLEVIEWHELPERS_DIR}/translations ${QTABLEVIEWHELPERS_TS_BASENAME} ${QTABLEVIEWHELPERS_SOURCES})

set(QTABLEVIEWHELPERS_GENFILES ${QTABLEVIEWHELPERS_GENFILES} ${QTABLEVIEWHELPERS_TRANSLATION_FILES})
set(QTABLEVIEWHELPERS_FILES ${QTABLEVIEWHELPERS_SOURCES} ${QTABLEVIEWHELPERS_GENFILES})

set(QTABLEVIEWHELPERS_INCLUDE_DIRS ${QTABLEVIEWHELPERS_DIR})
