<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>BoolBoxEditorDelegate</name>
    <message>
        <location filename="../table/delegates/BoolBoxEditorDelegate.h" line="12"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../table/delegates/BoolBoxEditorDelegate.h" line="12"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>TableCopyAction</name>
    <message>
        <location filename="../table/actions/TableCopyAction.cpp" line="65"/>
        <source>&amp;Copy</source>
        <translation>&amp;Копировать</translation>
    </message>
    <message>
        <location filename="../table/actions/TableCopyAction.cpp" line="66"/>
        <source>Copy selected text</source>
        <translation>Копировать выделенный текст</translation>
    </message>
</context>
</TS>
